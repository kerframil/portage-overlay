# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit acct-user

ACCT_USER_ID=750
ACCT_USER_GROUPS=( dogecoin )

acct-user_add_deps

pkg_setup() {
	ACCT_USER_HOME=/var/lib/dogecoin
}
