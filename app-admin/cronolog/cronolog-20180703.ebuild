# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit autotools

DESCRIPTION="Log rotation software"
HOMEPAGE="https://github.com/WayneD/cronolog"
EGIT_COMMIT="7516044fed04beb0ad34780f8e30d585a927760b"
SRC_URI="https://github.com/WayneD/cronolog/archive/${EGIT_COMMIT}.zip -> ${P}.zip"

LICENSE="GPL-2+ Apache-1.0"
SLOT="0"
KEYWORDS="amd64 ~arm ~ppc ~x86"

PATCHES=(
	"${FILESDIR}"/cronolog-doc.patch
	"${FILESDIR}"/cronolog-getopt-long.patch
)

S="${WORKDIR}/${PN}-${EGIT_COMMIT}"

src_prepare() {
	default
	eautoreconf
}
