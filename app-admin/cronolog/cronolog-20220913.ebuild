# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools

DESCRIPTION="Log rotation software"
HOMEPAGE="https://github.com/WayneD/cronolog"
EGIT_COMMIT="82413cdfc872aee72d6f1d5ff9d8877932adc425"
SRC_URI="https://github.com/WayneD/cronolog/${EGIT_COMMIT}.tar.gz -> ${P}.tar.gz"
RESTRICT="mirror"

LICENSE="GPL-2+ Apache-1.0"
SLOT="0"
KEYWORDS="amd64"

PATCHES=(
	"${FILESDIR}"/cronolog-doc.patch
	"${FILESDIR}"/cronolog-getopt-long.patch
)

S="${WORKDIR}/WayneD-${PN}-${EGIT_COMMIT:0:7}"

src_prepare() {
	default
	eautoreconf
}
