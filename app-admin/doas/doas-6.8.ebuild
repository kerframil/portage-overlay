# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="Run commands as super user or another user, alternative to sudo from OpenBSD"

HOMEPAGE="https://github.com/Duncaen/OpenDoas"
MY_PN=${HOMEPAGE##*/}
MY_P=${MY_PN,,}-${PV}
SRC_URI="https://github.com/Duncaen/${MY_PN}/releases/download/v${PV}/${MY_P}.tar.gz -> ${MY_P}.tar.gz"
S="${WORKDIR}"/${MY_P}

LICENSE="ISC"
SLOT="0"
KEYWORDS="amd64 ~arm ~arm64 ~ppc"
IUSE="pam persist"

RDEPEND="pam? ( sys-libs/pam )"
DEPEND="${RDEPEND}
	virtual/yacc"

src_configure() {
	tc-export CC AR
	./configure \
		--prefix="${EPREFIX}"/usr \
		--sysconfdir="${EPREFIX}"/etc \
		$(use_with pam) \
		$(use_with persist timestamp) \
		|| die
}
