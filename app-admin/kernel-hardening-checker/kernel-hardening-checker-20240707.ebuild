# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{9..12} )

inherit distutils-r1 git-r3

DESCRIPTION="A tool for checking the security hardening options of the Linux kernel"
HOMEPAGE="https://github.com/a13xp0p0v/kernel-hardening-checker"
LICENSE="GPL-3"
SLOT="0"

EGIT_REPO_URI="https://github.com/a13xp0p0v/kernel-hardening-checker.git"
EGIT_COMMIT="eb1f11a8f647309c7d7f425c8ef28ca82f6b3ba5"
KEYWORDS="amd64 ~x86"

distutils_enable_tests pytest
