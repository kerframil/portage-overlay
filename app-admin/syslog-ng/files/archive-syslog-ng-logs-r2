#!/bin/bash

# Copyright 2021 Kerin Millar <kfm@plushkava.net>

# Permission to use, copy, modify, and distribute this software for any purpose
# with or without fee is hereby granted, provided that the above copyright
# notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

zstd_args=(-9 -T0 --long --rm)

facilities=(
	auth
	authpriv
	cron
	daemon
	ftp
	kern
	lpr
	mail
	news
	security
	syslog
	user
	uucp
	local{0..7}
)

rotate() {
	local log=$1 ext
	if hash zstd 2>/dev/null; then
		ext=zst
		chrt -i 0 zstd "${zstd_args[@]}" "$log"
	else
		ext=gz
		chrt -i 0 gzip -9 "$log"
	fi &&
	if (( has_archive )); then
		mv -n -v "$log.$ext" /var/log/archive/
	fi
}

# Establish a mutex
lockfile="/run/archive-syslog-ng-logs.lock"
exec 9>"$lockfile"
flock -n 9 || exit
trap 'rm -f -- "$lockfile"' EXIT

# Silence STDOUT unless it is attached to a terminal
if ! test -t 1; then
	exec >/dev/null
fi

# Render zstd quiet unless STDERR is attached to a terminal
if ! test -t 2; then
	zstd_args+=(-q)
fi

# Determine whether an archive directory is present
test -d /var/log/archive
has_archive=$(( $? == 0 ))

# Obtain the current ISO year and week number
printf -v cur_year '%(%G)T' -1
printf -v cur_week '%(%V)T' -1
cur_week=${cur_week#0}

# Collect and compress eligible log files. This works for files created by
# syslog-ng using file("/var/log/${FACILITY}-${YEAR}W${ISOWEEK}.log").
shopt -s extglob nullglob
for f in "${facilities[@]}"; do
	for log in /var/log/"$f"-*.log; do
		if [[ ${log%.*} =~ ([0-9]{4})W([0-9]{2})$ ]]; then
			year=${BASH_REMATCH[1]} week=${BASH_REMATCH[2]} week=${week#0}
			if (( year < cur_year || (year == cur_year && week < cur_week) )); then
				rotate "$log" || exit
			fi
		fi
	done
	# Move any compressed log files that were created during prior runs to the
	# archive directory, if it exists. This handles the case whereby an admin
	# begins with no archive directory but subsequently decides to create one.
	if (( has_archive )); then
		for log in /var/log/"$f"-*.log.@(gz|zst); do
			mv -n -v "$log" /var/log/archive/ || exit
		done
	fi
done
