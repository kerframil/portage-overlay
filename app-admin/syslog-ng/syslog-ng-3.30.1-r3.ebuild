# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{7..9} )
inherit autotools python-single-r1 systemd

MY_PV_MM=$(ver_cut 1-2)
DESCRIPTION="syslog replacement with advanced filtering features"
HOMEPAGE="https://syslog-ng.com/open-source-log-management"
SRC_URI="https://github.com/balabit/syslog-ng/releases/download/${P}/${P}.tar.gz"

LICENSE="GPL-2+ LGPL-2.1+"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~arm ~arm64 ~hppa ~ia64 ~mips ~ppc ~ppc64 ~riscv ~s390 ~sparc ~x86"
IUSE="amqp caps dbi geoip2 http ipv6 json kafka mongodb pacct python redis smtp snmp test spoof-source systemd +zstd"
REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )"
RESTRICT="!test? ( test )"

RDEPEND="
	!dev-libs/eventlog
	>=dev-libs/glib-2.10.1:2
	>=dev-libs/ivykis-0.42.4
	>=dev-libs/libpcre-6.1:=
	dev-libs/openssl:0=
	amqp? ( >=net-libs/rabbitmq-c-0.8.0:=[ssl] )
	caps? ( sys-libs/libcap )
	dbi? ( >=dev-db/libdbi-0.9.0 )
	geoip2? ( dev-libs/libmaxminddb:= )
	http? ( net-misc/curl )
	json? ( >=dev-libs/json-c-0.9:= )
	kafka? ( >=dev-libs/librdkafka-1.0.0:= )
	mongodb? ( >=dev-libs/mongo-c-driver-1.2.0 )
	python? ( ${PYTHON_DEPS} )
	redis? ( >=dev-libs/hiredis-0.11.0:= )
	smtp? ( net-libs/libesmtp )
	snmp? ( net-analyzer/net-snmp:0= )
	spoof-source? ( net-libs/libnet:1.1= )
	systemd? ( sys-apps/systemd:= )
	zstd? ( >=app-arch/zstd-1.4.8 )"
DEPEND="${RDEPEND}
	test? ( dev-libs/criterion )"
BDEPEND="
	sys-devel/flex
	virtual/pkgconfig"

DOCS=(AUTHORS NEWS.md CONTRIBUTING.md contrib/syslog-ng.{conf.doc,vim})
PATCHES=(
	"${FILESDIR}"/${PN}-3.28.1-net-snmp.patch
)

pkg_setup() {
	use python && python-single-r1_pkg_setup
}

src_prepare() {
	local f

	use python && python_fix_shebang .

	# remove bundled libs
	rm -r lib/ivykis || die

	# drop scl modules requiring json
	if ! use json; then
		sed -i -r '/cim|elasticsearch|ewmm|graylog2|loggly|logmatic|netskope|nodejs|osquery|slack/d' scl/Makefile.am || die
	fi

	# drop scl modules requiring http
	if ! use http; then
		sed -i -r '/slack|telegram/d' scl/Makefile.am || die
	fi

	# use gentoo default path
	if use systemd; then
		sed -e 's@/etc/syslog-ng.conf@/etc/syslog-ng/syslog-ng.conf@g;s@/var/run@/run@g' \
			-i contrib/systemd/syslog-ng@default || die
	fi

	{ printf '@version: %s\n' "${MY_PV_MM}" && cat "${FILESDIR}/syslog-ng.conf"; } > "${T}/syslog-ng.conf" || die

	default
	eautoreconf
}

src_configure() {
	local myconf=(
		--disable-docs
		--disable-java
		--disable-java-modules
		--disable-riemann
		--disable-tcp-wrapper
		--enable-manpages
		--localstatedir=/var/lib/syslog-ng
		--sysconfdir=/etc/syslog-ng
		--with-embedded-crypto
		--with-ivykis=system
		--with-module-dir=/usr/$(get_libdir)/syslog-ng
		--with-pidfile-dir=/var/run
		--with-systemdsystemunitdir="$(systemd_get_systemunitdir)"
		$(use_enable amqp)
		$(usex amqp --with-librabbitmq-client=system --without-librabbitmq-client)
		$(use_enable caps linux-caps)
		$(use_enable dbi sql)
		$(use_enable geoip2)
		$(use_enable http)
		$(use_enable ipv6)
		$(use_enable json)
		$(use_enable kafka)
		$(use_enable mongodb)
		$(usex mongodb --with-mongoc=system "--without-mongoc --disable-legacy-mongodb-options")
		$(use_enable pacct)
		$(use_enable python)
		$(use_enable redis)
		$(use_enable smtp)
		$(use_enable snmp afsnmp)
		$(use_enable spoof-source)
		$(use_enable systemd)
	)

	econf "${myconf[@]}"
}

src_install() {
	default

	# Install default configuration
	insinto /etc/default
	doins contrib/systemd/syslog-ng@default

	insinto /etc/syslog-ng
	newins "${T}/syslog-ng.conf" syslog-ng.conf

	newinitd "${FILESDIR}/syslog-ng.initd" syslog-ng
	newconfd "${FILESDIR}/syslog-ng.confd-r1" syslog-ng

	exeinto /etc/cron.daily
	newexe "${FILESDIR}/archive-syslog-ng-logs-r2" archive-syslog-ng-logs

	keepdir /etc/syslog-ng/patterndb.d /var/lib/syslog-ng
	find "${D}" -name '*.la' -delete || die

	use python && python_optimize
}

pkg_postinst() {
	if use systemd; then
		ewarn "The service file for systemd has changed to support multiple instances."
		ewarn "To start the default instance issue:"
		ewarn "# systemctl start syslog-ng@default"
	fi
}
