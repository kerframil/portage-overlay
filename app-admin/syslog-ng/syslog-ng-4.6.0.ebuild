# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )
inherit autotools python-single-r1 systemd

DESCRIPTION="syslog replacement with advanced filtering features"
HOMEPAGE="https://www.syslog-ng.com/products/open-source-log-management/"
SRC_URI="https://github.com/balabit/syslog-ng/releases/download/${P}/${P}.tar.gz"

LICENSE="GPL-2+ LGPL-2.1+"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 hppa ~loong ~mips ppc ppc64 ~riscv ~s390 sparc x86"
IUSE="amqp caps +cron dbi geoip2 grpc http json kafka mongodb pacct python redis smtp snmp test spoof-source systemd +zstd"
REQUIRED_USE="python? ( ${PYTHON_REQUIRED_USE} )
	test? ( python )"
RESTRICT="!test? ( test )"

RDEPEND="
	>=dev-libs/glib-2.10.1:2
	>=dev-libs/ivykis-0.42.4
	>=dev-libs/libpcre2-10.0
	dev-libs/openssl:0=
	!dev-libs/eventlog
	amqp? ( >=net-libs/rabbitmq-c-0.8.0:=[ssl] )
	caps? ( sys-libs/libcap )
	dbi? ( >=dev-db/libdbi-0.9.0 )
	geoip2? ( dev-libs/libmaxminddb:= )
	grpc? (
		dev-libs/protobuf:=
		net-libs/grpc:=
	)
	http? ( net-misc/curl )
	json? ( >=dev-libs/json-c-0.9:= )
	kafka? ( >=dev-libs/librdkafka-1.0.0:= )
	mongodb? ( >=dev-libs/mongo-c-driver-1.2.0 )
	python? (
		${PYTHON_DEPS}
		$(python_gen_cond_dep '
			dev-python/setuptools[${PYTHON_USEDEP}]
		')
	)
	redis? ( >=dev-libs/hiredis-0.11.0:= )
	smtp? ( net-libs/libesmtp:= )
	snmp? ( net-analyzer/net-snmp:0= )
	spoof-source? ( net-libs/libnet:1.1 )
	systemd? ( sys-apps/systemd:= )
	zstd? ( >=app-arch/zstd-1.4.8 )"
DEPEND="${RDEPEND}
	test? ( dev-libs/criterion )"
BDEPEND="
	sys-apps/ed
	>=sys-devel/bison-3.7.6
	sys-devel/flex
	virtual/pkgconfig
	grpc? ( dev-libs/protobuf:= )"

DOCS=(AUTHORS NEWS.md CONTRIBUTING.md contrib/syslog-ng.conf.doc)
PATCHES=(
	"${FILESDIR}"/${PN}-3.28.1-net-snmp.patch
)

pkg_setup() {
	use python && python-single-r1_pkg_setup
}

src_prepare() {
	local module modules=()

	# disable python-modules test as it requires additional python modules not
	# packaged in Gentoo
	printf '%s\n' H 'g/MAKE/c\' 'exit 0' w q \
	| ed -s modules/python-modules/test_pymodules.sh || die

	use python && python_fix_shebang .

	# remove bundled libs
	rm -r lib/ivykis || die

	# drop scl modules requiring json
	if use !json; then
		for module in \
			cim elasticsearch ewmm graylog2 loggly logmatic netskope nodejs \
			osquery slack
		do
			modules+=("g/$module/d")
		done
	fi

	# drop scl modules requiring http
	if use !http; then
		modules=('g/telegram/d')
		if use json; then
			modules+=('g/slack/d')
		fi
	fi

	if (( ${#modules[@]} )); then
		printf '%s\n' H "${modules[@]}" w q \
		| ed -s scl/Makefile.am || die
	fi

	# use /run in accordance with hier(7)
	if use systemd; then
		printf '%s\n' H 'g/\/var\/run/s//\/run/' w q \
		| ed -s contrib/systemd/syslog-ng@default || die
	fi

	# generate syslog-ng.conf
	{
		printf '@version: %s\n' "$(ver_cut 1-2)" &&
		cat -- "${FILESDIR}/syslog-ng.conf"
	} > "${T}/syslog-ng.conf" || die

	default
	eautoreconf
}

src_configure() {
	# Needs bison/flex.
	unset YACC LEX

	local myconf=(
		--disable-docs
		--disable-java
		--disable-java-modules
		--disable-riemann
		--enable-ipv6
		--enable-manpages
		--localstatedir=/var/lib/syslog-ng
		--sysconfdir=/etc/syslog-ng
		--with-embedded-crypto
		--with-ivykis=system
		--with-module-dir=/usr/$(get_libdir)/syslog-ng
		--with-pidfile-dir=/var/run
		--with-python-packages=none
		--with-systemdsystemunitdir="$(systemd_get_systemunitdir)"
		$(use_enable amqp)
		$(use_with amqp librabbitmq-client system)
		$(use_enable caps linux-caps)
		$(use_enable dbi sql)
		$(use_enable geoip2)
		$(use_enable grpc)
		$(use_enable grpc cpp)
		$(use_enable http)
		$(use_enable json)
		$(use_enable kafka)
		$(use_enable mongodb)
		$(usex mongodb --with-mongoc=system "--without-mongoc --disable-legacy-mongodb-options")
		$(use_enable pacct)
		$(use_enable python)
		$(use_enable redis)
		$(use_enable smtp)
		$(use_enable snmp afsnmp)
		$(use_enable spoof-source)
		$(use_enable systemd)
		--disable-tcp-wrapper
	)

	econf "${myconf[@]}"
}

src_install() {
	default

	# Install default configuration
	insinto /etc/default
	doins contrib/systemd/syslog-ng@default

	insinto /etc/syslog-ng
	newins "${T}/syslog-ng.conf" syslog-ng.conf

	if use cron; then
		exeinto /etc/cron.daily
		newexe "${FILESDIR}/archive-syslog-ng-logs-r3" archive-syslog-ng-logs
	fi

	newinitd "${FILESDIR}/syslog-ng.rc" syslog-ng
	newconfd "${FILESDIR}/syslog-ng.confd-r1" syslog-ng
	keepdir /etc/syslog-ng/patterndb.d /var/lib/syslog-ng
	find "${D}" -name '*.la' -delete || die

	use python && python_optimize
}
