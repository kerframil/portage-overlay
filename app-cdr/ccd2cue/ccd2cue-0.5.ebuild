# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="CCD (CloneCD) sheet to CUE sheet converter"
HOMEPAGE="https://www.gnu.org/software/ccd2cue/"
SRC_URI="mirror://gnu/${PN}/${P}.tar.gz"
RESTRICT="nomirror"

LICENSE="FDL-1.3 GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="nls"

src_configure() {
	econf --disable-doxygen-doc $(use_enable nls)
}
