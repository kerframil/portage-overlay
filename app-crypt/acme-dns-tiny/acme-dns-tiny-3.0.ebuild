# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{8,9,10,11} )
inherit python-single-r1

DESCRIPTION="A fork of acme-tiny that supports ACME DNS validation"
HOMEPAGE="https://projects.adorsaz.ch/adrien/acme-dns-tiny"
SRC_URI="https://projects.adorsaz.ch/adrien/acme-dns-tiny/-/archive/v${PV}/acme-dns-tiny-v${PV}.tar.gz"
RESTRICT="mirror"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~arm64 ~hppa ~loong ~m68k ~mips ~ppc ~ppc64 ~riscv ~s390 ~sparc ~x86"
IUSE=""
REQUIRED_USE="${PYTHON_REQUIRED_USE}"

RDEPEND="
	${PYTHON_DEPS}
	dev-libs/openssl:0
	>=dev-python/dnspython-1.16
"

S=${WORKDIR}/${PN}-v${PV}

src_compile() {
	:
}

src_install() {
	python_newexe acme_dns_tiny.py acme-dns-tiny
	dodoc *.md documentations/*.md example.ini
}
