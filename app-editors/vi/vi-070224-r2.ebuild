# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="The original ex/vi text editor"
HOMEPAGE="http://ex-vi.sourceforge.net/"
SRC_URI="https://sources.archlinux.org/other/$PN/ex-$PV.tar.xz"
RESTRICT="mirror"

LICENSE="BSD-4"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="sys-libs/ncurses"
RDEPEND="${DEPEND}"

S="$WORKDIR/ex-$PV"

PATCHES=(
	"$FILESDIR/fix-tubesize-short-overflow.patch"
	"$FILESDIR/navkeys.patch"
	"$FILESDIR/format-security.patch"
	"$FILESDIR/linenum.patch"
	"$FILESDIR/preserve-dir.patch"
)

src_compile() {
	local ncurses
	ncurses=$("$(tc-getPKG_CONFIG)" --libs ncurses)
	emake -j1 \
		PREFIX=/usr \
		LIBEXECDIR="/usr/$(get_libdir)/ex" \
		PRESERVEDIR=/var/lib/ex \
		TERMLIB="${ncurses#-l}" \
		FEATURES="-DCHDIR -DFASTTAG -DUCVISUAL -DMB -DBIT8" \
		LARGEF=-DLARGEF
}

src_install() {
	keepdir /var/lib/ex
	emake -j1 \
		PREFIX=/usr \
		LIBEXECDIR="/usr/$(get_libdir)/ex" \
		PRESERVEDIR=/var/lib/ex \
		INSTALL=/usr/bin/install \
		DESTDIR="$D" \
		STRIP= install
	(
		set -e
		cd "$D"/usr/bin
		mv ex ex-ex
		for f in vi view; do
			rm "$f"
			ln -s ex-ex ex-"$f"
		done
	)
}
