# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=(python2_7)

inherit python-single-r1

MY_COMMIT=dcbcfd2a5421

DESCRIPTION="A simple command-line tool to manage qemu-kvm guests"
HOMEPAGE="https://www.linux-kvm.org/page/Kvmtools"
SRC_URI="https://hg.kasten-edv.de/${PN}/archive/${MY_COMMIT}.tar.bz2 -> ${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE=""

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="${PYTHON_DEPS}"
RDEPEND="${PYTHON_DEPS} app-emulation/qemu"

S="${WORKDIR}/${PN}-${MY_COMMIT}"

src_install() {
	python_domodule kvmtools
	dobin bin/*
	keepdir /etc/kvm/auto
	insinto /etc/kvm
	doins -r config domains scripts
	dodoc README.txt docs/*
}
