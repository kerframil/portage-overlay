# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

PYTHON_COMPAT=(python2_7)

inherit python-single-r1 git-r3

DESCRIPTION="A simple command-line tool to manage qemu-kvm guests"
HOMEPAGE="https://github.com/kerframil/kvm-tools"
EGIT_REPO_URI="${HOMEPAGE}"
EGIT_COMMIT="f7fd730"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64"
IUSE=""

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

DEPEND="${PYTHON_DEPS}"
RDEPEND="${PYTHON_DEPS} app-emulation/qemu"

src_install() {
	python_domodule kvmtools
	dobin bin/*
	keepdir /etc/kvm/auto
	insinto /etc/kvm
	doins -r config domains scripts
	dodoc README.txt docs/*
}
