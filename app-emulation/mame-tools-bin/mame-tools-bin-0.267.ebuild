# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Tools provided by the Multiple Arcade Machine Emulator"
HOMEPAGE="https://archlinux.org/packages/extra/x86_64/mame-tools/"
SRC_URI="https://mirror.pkgbuild.com/extra/os/x86_64/mame-tools-${PV}-1-x86_64.pkg.tar.zst"
LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64"

BDEPEND="
	app-arch/zstd
"
RDEPEND="
	dev-libs/libutf8proc
	media-libs/flac:0/10-12
	media-libs/libogg
	media-libs/libsdl2
	sys-devel/gcc:14
	sys-libs/zlib
"

S=${WORKDIR}

src_unpack() {
	zstdcat "${DISTDIR}/mame-tools-${PV}-1-x86_64.pkg.tar.zst" | tar -x || die
}

src_install() {
	insinto usr/share/man
	doins -r usr/share/man/man1
	insopts -m0755
	insinto usr
	doins -r usr/bin
}
