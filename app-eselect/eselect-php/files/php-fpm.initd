#!/sbin/openrc-run

# We support both slot-agnostic and slotted versions of the init script. The
# slotted versions would be named something like php-fpm-php7.1, and PHP_SLOT
# below would be set to "php7.1". But we also support a general init script
# named "php-fpm" that uses whatever the currently-eselected fpm implementation
# is. In that case, PHP_SLOT winds up set to "php-fpm" and we need to get the
# actual slot by querying eselect.
#
case $RC_SVCNAME in
	php-fpm)
		php_slot=$(eselect php show fpm)
		;;
	php-fpm-?*)
		php_slot=${RC_SVCNAME#php-fpm-}
		;;
	*)
		false
		;;
esac || {
	eerror "Failed to determine the intended PHP slot"
	exit 1
}

supervisor="supervise-daemon"
command="/usr/lib64/$php_slot/bin/php-fpm"
php_fpm_conf="/etc/php/fpm-$php_slot/php-fpm.conf"
extra_started_commands="reload"
extra_commands="configtest"

configtest() (
	ebegin "Testing $RC_SVCNAME configuration"
	if ! output=$("$command" -y "$php_fpm_conf" -t 2>&1); then
		printf '%s\n' "$output" >&2
		false
	fi
	eend $?
)

start_pre() {
	if [ "$RC_CMD" != "restart" ]; then
		configtest
	fi
}

start() {
	ebegin "Starting $RC_SVCNAME"
	supervise-daemon "$RC_SVCNAME" -m 0 ${umask:+-k "$umask"} -S -- \
		"$command" -y "$php_fpm_conf" -F
	eend $?
}

stop_pre() {
	if [ "$RC_CMD" = "restart" ]; then
		configtest
	fi
}

reload() {
	configtest || return
	ebegin "Reloading $RC_SVCNAME"
		supervise-daemon "$RC_SVCNAME" -s USR2
	eend $?
}
