# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A json parser for the shell"
HOMEPAGE="http://kmkeen.com/jshon/"
SRC_URI="http://kmkeen.com/$PN/$PN-$PV.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="dev-libs/jansson"
RDEPEND="${DEPEND}"
