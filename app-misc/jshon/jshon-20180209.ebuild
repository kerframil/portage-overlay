# Copyright 1999-2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

MY_COMMIT_HASH="d919aeaece37962251dbe6c1ee50f0028a5c90e4"

DESCRIPTION="A json parser for the shell"
HOMEPAGE="http://kmkeen.com/jshon/"
SRC_URI="https://github.com/keenerd/${PN}/tarball/${MY_COMMIT_HASH} -> ${P}.tar.gz"
RESTRICT="mirror"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="dev-libs/jansson"
RDEPEND="${DEPEND}"

S="${WORKDIR}/keenerd-${PN}-${MY_COMMIT_HASH:0:7}"

src_install() {
	default
	rm -r "${ED}/usr/share/zsh" || die
}
