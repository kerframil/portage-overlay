# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="A fast CSV command line toolkit written in Rust"
HOMEPAGE="https://github.com/BurntSushi/xsv"
SRC_URI="https://github.com/BurntSushi/xsv/releases/download/${PV}/xsv-${PV}-x86_64-unknown-linux-musl.tar.gz"

LICENSE="|| ( MIT Unlicense ) Apache-2.0 Boost-1.0 MIT Unlicense"
SLOT="0"
KEYWORDS="amd64"
IUSE="test"
RESTRICT="
	mirror
	!test? ( test )
"

RDEPEND="!sci-calculators/xsv"

S=${WORKDIR}

src_test() {
	local -a lines
	mapfile -t lines < <(
		printf '%s\n' col1,col2,col3 'foo,"bar baz",quux' \
		| ./xsv select 2
	)
	wait $! && [[ ${lines[0]} == col2 && ${lines[1]} == "bar baz" ]]
}

src_install() {
	dobin xsv
}
