# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit vim-plugin

DESCRIPTION="vim plugin: syntax highlighting for dhcpd.conf"
HOMEPAGE="https://www.vim.org/scripts/script.php?script_id=744"
SRC_URI="https://github.com/vim-scripts/dhcpd.vim/archive/460075c2ad857b254340af3c17ba13e2d2cb99cf.zip"

LICENSE="vim"
KEYWORDS="alpha amd64 arm arm64 hppa loong m68k mips ppc ppc64 riscv s390 sparc x86"

VIM_PLUGIN_HELPTEXT="This plugin provides syntax highlighting for dhcpd.conf files."

PATCHES=( "${FILESDIR}"/"${P}"-multiple-addresses.patch )

S=${WORKDIR}/dhcpd.vim-460075c2ad857b254340af3c17ba13e2d2cb99cf
