# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit vim-plugin

COMMIT_ID="73b9ef5dcb6cdf6488bc88adb382f20bc3e3262a"

DESCRIPTION="Forget Vim tabs – now you can have buffer tabs"
HOMEPAGE="https://github.com/ap/vim-buftabline"
SRC_URI="https://github.com/ap/${PN}/archive/${COMMIT_ID}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}-${COMMIT_ID}"

LICENSE="MIT"
KEYWORDS="amd64"
