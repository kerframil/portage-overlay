# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit vim-plugin

COMMIT_ID="d1cf31e5215ad6f5544d9e631383aef4b6c35cb7"

DESCRIPTION="vim plugin: a simplified and optimized Gruvbox colorscheme for Vim"
HOMEPAGE="https://github.com/lifepillar/vim-gruvbox8"
SRC_URI="https://github.com/lifepillar/${PN}/archive/${COMMIT_ID}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}-${COMMIT_ID}"

LICENSE="MIT"
KEYWORDS="amd64 ~x86"
