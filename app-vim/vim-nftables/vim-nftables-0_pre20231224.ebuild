# Copyright 2020-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit vim-plugin

COMMIT_ID="bc29309080b4c7e1888ffb1a830846be16e5b8e7"

DESCRIPTION="vim plugin: nftables syntax and indent"
HOMEPAGE="https://github.com/awisse/vim-nftables"
SRC_URI="https://github.com/awisse/${PN}/archive/${COMMIT_ID}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}-${COMMIT_ID}"

LICENSE="MIT"
KEYWORDS="amd64 ~x86"
