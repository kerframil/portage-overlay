# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit vim-plugin

COMMIT_ID="34533e9086b0249e5789a5d70298a6d1dfd3e6d5"

DESCRIPTION="vim plugin: syslog-ng syntax highlighting for vim"
HOMEPAGE="https://github.com/syslog-ng/vim-syslog-ng"
SRC_URI="https://github.com/syslog-ng/${PN}/archive/${COMMIT_ID}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}-${COMMIT_ID}"

LICENSE="MIT"
KEYWORDS="amd64"
