# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit vim-plugin

COMMIT_ID="d36caa6b1cf508a4df1c691f915572fc02143258"

DESCRIPTION="vim plugin: Vim syntax for TOML"
HOMEPAGE="https://github.com/cespare/vim-toml"
SRC_URI="https://github.com/cespare/${PN}/archive/${COMMIT_ID}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}-${COMMIT_ID}"

LICENSE="MIT"
KEYWORDS="amd64 ~x86"
