# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit vim-plugin

COMMIT_ID="5540b3faa2288b226a8d9a4e8244558b12c598aa"

DESCRIPTION="vim plugin: highlights trailing whitespace in red"
HOMEPAGE="https://github.com/bronson/vim-trailing-whitespace"
SRC_URI="https://github.com/bronson/${PN}/archive/${COMMIT_ID}.tar.gz -> ${P}.tar.gz"
S="${WORKDIR}/${PN}-${COMMIT_ID}"

LICENSE="MIT"
KEYWORDS="amd64 ~x86"
