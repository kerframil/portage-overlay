# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit eapi7-ver java-vm-2

SLOT=$(ver_cut 1)
MY_PV="1.${SLOT}.0-u$(ver_cut 2)"

DESCRIPTION="Prebuilt Java JDK binaries provided by Alex Kashchenko"
HOMEPAGE="https://bitbucket.org/alexkasko/openjdk-unofficial-builds/"
SRC_URI="https://bitbucket.org/alexkasko/openjdk-unofficial-builds/downloads/openjdk-${MY_PV}-unofficial-linux-amd64-image.zip"

echo "$SRC_URI"
LICENSE="GPL-2-with-classpath-exception"
KEYWORDS="~amd64"

IUSE="alsa cups headless-awt selinux source"

RDEPEND="
	media-libs/fontconfig:1.0
	media-libs/freetype:2
	>=sys-apps/baselayout-java-0.1.0-r1
	>=sys-libs/glibc-2.2.5:*
	sys-libs/zlib
	alsa? ( media-libs/alsa-lib )
	cups? ( net-print/cups )
	selinux? ( sec-policy/selinux-java )
	!headless-awt? (
		x11-libs/libX11
		x11-libs/libXext
		x11-libs/libXi
		x11-libs/libXrender
		x11-libs/libXtst
	)
"

RESTRICT="preserve-libs strip"
QA_PREBUILT="*"

S="${WORKDIR}/openjdk-${MY_PV}-unofficial-linux-amd64-image"

src_install() {
	local dest="/opt/${P}"
	local ddest="${ED%/}/${dest#/}"

	rm ASSEMBLY_EXCEPTION LICENSE THIRD_PARTY_README || die

	if ! use alsa ; then
		rm -v jre/lib/*/libjsoundalsa.so* || die
	fi

	if use headless-awt ; then
		rm -vr jre/lib/*/lib{jawt,splashscreen}.so \
			{,jre/}bin/policytool bin/appletviewer || die
	fi

	if ! use source ; then
		rm -v src.zip || die
	fi

	rm -v jre/lib/security/cacerts || die
	dosym ../../../../../etc/ssl/certs/java/cacerts \
		"${dest}"/jre/lib/security/cacerts

	dodir "${dest}"
	cp -pPR * "${ddest}" || die

	java-vm_install-env "${FILESDIR}"/${PN}-${SLOT}.env.sh
	java-vm_set-pax-markings "${ddest}"
	java-vm_revdep-mask
	java-vm_sandbox-predict /dev/random /proc/self/coredump_filter
}
