# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Convenience functions for use in bash scripts"
HOMEPAGE="https://to.lhunath.com/bashlib"
SRC_URI="https://raw.githubusercontent.com/lhunath/scripts/1f94eab/bashlib/bashlib"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"

S=$WORKDIR

src_unpack() {
	cp "$DISTDIR/bashlib" "$S"/
}

src_install() {
	insinto /usr/share/bashlib
	doins bashlib
}
