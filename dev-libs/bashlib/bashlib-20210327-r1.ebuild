# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Convenience functions for use in bash scripts"
HOMEPAGE="https://github.com/lhunath/scripts"
SRC_URI="https://raw.githubusercontent.com/lhunath/scripts/26a67b971382bc93fe8b51bc4a00439d14fc8199/bashlib/bashlib -> $P"
RESTRICT="mirror"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~arm64 ~hppa ~loong ~m68k ~mips ~ppc ~ppc64 ~riscv ~s390 ~sparc ~x86 ~x64-cygwin ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"

S=$WORKDIR

src_unpack() {
	cp --reflink=auto "$DISTDIR/$P" "$S/$PN"
}

src_install() {
	insinto "/usr/share/$PN"
	doins "$PN"
}
