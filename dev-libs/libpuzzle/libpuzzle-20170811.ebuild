# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

inherit git-r3 autotools

DESCRIPTION="A library designed to quickly find visually similar images"
HOMEPAGE="https://libpuzzle.pureftpd.org/project/libpuzzle"
EGIT_REPO_URI="https://github.com/jedisct1/libpuzzle"
EGIT_COMMIT="a1023d7"

LICENSE="ISC"
SLOT="0"
KEYWORDS="~amd64 ~x86"

RDEPEND="media-libs/gd"
DEPEND="${RDEPEND}"

DOCS=(AUTHORS COPYING README README-PHP THANKS)

src_prepare() {
	eautoreconf
	eapply_user
}

src_install() {
	default
	doman man/*.[38]
}
