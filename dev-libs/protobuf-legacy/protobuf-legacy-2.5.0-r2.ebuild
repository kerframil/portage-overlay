# Copyright 1999-2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

inherit autotools flag-o-matic multilib-minimal toolchain-funcs

DESCRIPTION="Google's Protocol Buffers - Extensible mechanism for serializing structured data"
HOMEPAGE="https://developers.google.com/protocol-buffers/ https://github.com/protocolbuffers/protobuf"
MY_P="protobuf-${PV}"
SRC_URI="https://github.com/google/protobuf/releases/download/v${PV}/${MY_P}.tar.bz2"
RESTRICT="mirror"

LICENSE="BSD"
SLOT="${PV%.*}"
KEYWORDS="amd64"
IUSE="static-libs zlib"

DEPEND="zlib? ( sys-libs/zlib[${MULTILIB_USEDEP}] )"

S="${WORKDIR}/${MY_P}"

src_prepare() {
	default
	eapply -p0 "${FILESDIR}/protobuf-2.3.0-asneeded-2.patch"
	eapply -p1 "${FILESDIR}/protobuf-2.6.1-protoc-cmdline.patch"
	eautoreconf
}

src_configure() {
	append-cppflags -DGOOGLE_PROTOBUF_NO_RTTI

	if tc-ld-is-gold; then
		# https://sourceware.org/bugzilla/show_bug.cgi?id=24527
		tc-ld-disable-gold
	fi

	multilib-minimal_src_configure
}

multilib_src_configure() {
	local options=(
		OBJC="$(tc-getBUILD_CC)"
		$(use_enable static-libs static)
		$(use_with zlib)
		--libdir="${EPREFIX}/usr/$(get_libdir)/protobuf${PV%.*}"
	)

	if tc-is-cross-compiler; then
		# Build system uses protoc when building, so protoc copy runnable on host is needed.
		mkdir -p "${WORKDIR}/build" || die
		pushd "${WORKDIR}/build" > /dev/null || die
		ECONF_SOURCE="${S}" econf_build "${options[@]}"
		options+=(--with-protoc="$(pwd)/src/protoc")
		popd > /dev/null || die
	fi

	ECONF_SOURCE="${S}" econf "${options[@]}"
}

src_compile() {
	multilib-minimal_src_compile
}

multilib_src_compile() {
	if tc-is-cross-compiler; then
		emake -C "${WORKDIR}/build/src" protoc
	fi

	default
}

multilib_src_test() {
	emake check
}

multilib_src_install() {
	default
	# Modify some of the paths so as not to overlap with the
	# dev-libs/protobuf package. Also, install a wrapper script under
	# /opt/protobuf${major}/bin. That way, the older version may be used
	# simply by prepending said path to PATH.
	cd "${ED:?}" &&
	if multilib_is_native_abi; then
		local major=${PV%.*}
		local wrapper="opt/protobuf${major}/bin/protoc"
		mv usr/bin/protoc{,"$major"} \
		&& mkdir -p "${wrapper%/*}" \
		&& printf '#!/bin/sh\nexec /usr/bin/protoc%s "$@"\n' "$major" > "$wrapper" \
		&& chmod +x "$wrapper"
	else
		rm -r usr/bin
	fi || die
}

multilib_src_install_all() {
	dodoc *.txt
	cd "${ED:?}" \
	&& rm -r usr/include \
	&& find . -name "*.la" -o -name "*.pc" -delete \
	&& find . -type d -empty -delete \
	|| die
}
