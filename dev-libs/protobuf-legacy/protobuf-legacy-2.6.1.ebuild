# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5
AUTOTOOLS_AUTORECONF=1
AUTOTOOLS_PRUNE_LIBTOOL_FILES=all

inherit autotools-multilib eutils flag-o-matic toolchain-funcs

DESCRIPTION="Google's Protocol Buffers -- an efficient method of encoding structured data"
HOMEPAGE="https://code.google.com/p/protobuf/ https://github.com/google/protobuf/"
MY_P="protobuf-${PV}"
SRC_URI="https://github.com/google/protobuf/releases/download/v${PV}/${MY_P}.tar.bz2"

LICENSE="BSD"
SLOT="0/9" # subslot = soname major version
KEYWORDS="amd64"
IUSE="static-libs zlib"

CDEPEND="zlib? ( >=sys-libs/zlib-1.2.8-r1[${MULTILIB_USEDEP}] )"
DEPEND="${CDEPEND}"
RDEPEND="${CDEPEND}"

S="${WORKDIR}/${MY_P}"

src_prepare() {
	append-cxxflags -DGOOGLE_PROTOBUF_NO_RTTI
	epatch "${FILESDIR}/protobuf-2.3.0-asneeded-2.patch"
	epatch "${FILESDIR}/protobuf-2.6.1-protoc-cmdline.patch"
	autotools-multilib_src_prepare
}

src_configure() {
	local -a myeconfargs=(
		--libdir="${EPREFIX}/usr/$(get_libdir)/protobuf${PV%%.*}"
		$(use_with zlib)
	)
	if tc-is-cross-compiler; then
		# The build system wants `protoc` when building, so we need a copy that
		# runs on the host.  This is more hermetic than relying on the version
		# installed in the host being the exact same version.
		mkdir -p "${WORKDIR}"/build || die
		pushd "${WORKDIR}"/build >/dev/null
		ECONF_SOURCE=${S} econf_build "${myeconfargs[@]}"
		myeconfargs+=( --with-protoc="${PWD}"/src/protoc )
		popd >/dev/null
	fi
	autotools-multilib_src_configure
}

multilib_src_compile() {
	default
}

src_compile() {
	if tc-is-cross-compiler; then
		emake -C "${WORKDIR}"/build/src protoc
	fi
	autotools-multilib_src_compile
}

src_test() {
	autotools-multilib_src_test check
}

src_install() {
	autotools-multilib_src_install
	(
		# Modify some of the paths so as not to overlap with the
		# dev-libs/protobuf package. Also, install a wrapper script under
		# /opt/protobuf${major}/bin. That way, the older version may be used
		# simply by prepending said path to PATH.
		major=${PV%%.*}
		wrapper="opt/protobuf${major}/bin/protoc"
		cd -- "${ED}" &&
			mv usr/bin/protoc{,"$major"} &&
			mv usr/include/google/protobuf{,"$major"} &&
			mkdir -p "${wrapper%/*}" &&
			printf '#!/bin/sh\nexec /usr/bin/protoc%s "$@"\n' "${major}" > "${wrapper}" &&
			chmod +x "${wrapper}"
	) || die
	dodoc CHANGES.txt CONTRIBUTORS.txt README.md
}
