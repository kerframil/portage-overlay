# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=HERMES
DIST_VERSION=0.010
inherit perl-module

DESCRIPTION="Choose appropriate clone utility"

SLOT="0"
KEYWORDS="amd64 ~hppa ~ppc ~x86"
IUSE="minimal test"
RESTRICT="!test? ( test )"

RDEPEND="
	!minimal? (
		dev-perl/Clone
		dev-perl/Clone-PP
		dev-perl/Module-Runtime
	)
"
DEPEND="${RDEPEND}
	test? (
		dev-perl/Test-Without-Module
		>=virtual/perl-Test-Simple-0.900.0
	)
"
