# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DIST_AUTHOR=HERMES
DIST_VERSION=0.010
inherit perl-module

DESCRIPTION="Choose appropriate clone utility"

SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86 ~ppc-macos ~x86-solaris"
IUSE="minimal test"

RDEPEND="
	!minimal? (
		dev-perl/Clone
		dev-perl/Clone-PP
	)
	virtual/perl-Storable"
DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? (
		dev-perl/Module-Runtime
		dev-perl/Test-Without-Module
		virtual/perl-Test-Simple
	)"
