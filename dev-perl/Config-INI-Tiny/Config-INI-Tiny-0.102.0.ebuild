# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=ARISTOTLE
DIST_VERSION=0.102
inherit perl-module

DESCRIPTION="Parse INI configuration in extremely little code"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="virtual/perl-Carp"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? ( virtual/perl-Test-Simple )
"
