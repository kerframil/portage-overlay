# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=ARISTOTLE
DIST_VERSION=0.105
inherit perl-module

DESCRIPTION="Parse INI configuration in extremely little code"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
