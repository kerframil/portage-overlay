# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=DBOOK
DIST_VERSION=0.002
inherit perl-module

DESCRIPTION="Access or set data in deep structures"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"
RESTRICT="mirror"

RDEPEND="
	dev-perl/Sentinel
"
DEPEND="${RDEPEND}
	test? (
		dev-perl/Test2-Suite
	)
"
