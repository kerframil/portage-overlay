# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
DIST_AUTHOR=MAUKE
DIST_VERSION=0.097
inherit perl-module

DESCRIPTION="Various utility functions"

SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="virtual/perl-Exporter"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	virtual/perl-File-Spec
	test? ( dev-perl/Test-Warnings )
"
