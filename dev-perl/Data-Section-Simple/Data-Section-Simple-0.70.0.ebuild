# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6
DIST_AUTHOR=MIYAGAWA
DIST_VERSION=0.07
inherit perl-module

DESCRIPTION="Read data from __DATA__"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="virtual/perl-Exporter"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? ( dev-perl/Test-Requires )
"
