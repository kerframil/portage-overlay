# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=HAARG
DIST_VERSION=0.009004
inherit perl-module

DESCRIPTION="Include stack traces on all warnings and errors"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
