# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MJD
DIST_VERSION=0.12
inherit perl-module

DESCRIPTION="Print out each line before it is executed (like sh -x)"
LICENSE="public-domain"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="virtual/perl-ExtUtils-MakeMaker"
