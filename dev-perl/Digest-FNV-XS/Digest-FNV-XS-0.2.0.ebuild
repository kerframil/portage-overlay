# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MLEHMANN
DIST_VERSION=0.02
inherit perl-module

DESCRIPTION="Fowler/Noll/Vo (FNV) hashes"
LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="
	dev-perl/Canary-Stability
	virtual/perl-ExtUtils-MakeMaker
"
