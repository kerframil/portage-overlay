# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=DMAKI
DIST_VERSION=0.03
inherit perl-module

DESCRIPTION="A MurmurHash3 implementation for Perl"
LICENSE="all-rights-reserved" # the license is unknown upstream
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	>=dev-lang/perl-5.18
"
DEPEND="${RDEPEND}
	dev-perl/Module-Install
"
