# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=DMAKI
DIST_VERSION=0.03
inherit perl-module

DESCRIPTION="A MurmurHash3 implementation for Perl"
LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	virtual/perl-XSLoader
"

DEPEND="${RDEPEND}
	dev-perl/Module-Install
	virtual/perl-Devel-PPPort
	virtual/perl-ExtUtils-MakeMaker
	virtual/perl-ExtUtils-ParseXS
	test? ( virtual/perl-Test-Simple )
"
