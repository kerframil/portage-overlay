# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MAUKE
DIST_VERSION=0.11
inherit perl-module

DESCRIPTION="A __DIR__ constant for the directory your source file is in"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	virtual/perl-Carp
	virtual/perl-File-Spec
"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
"
