# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=DBOOK
DIST_VERSION=1.000
inherit perl-module

DESCRIPTION="Encode and decode text, simply"
LICENSE="|| ( Artistic-2 GPL-1+ )"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test +xs"

RDEPEND="
	virtual/perl-Carp
	>=virtual/perl-Encode-2.85
	>=virtual/perl-Exporter-5.57
	xs? ( dev-perl/Unicode-UTF8 )
"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? (
		virtual/perl-File-Spec
		virtual/perl-Test-Simple
	)
"
