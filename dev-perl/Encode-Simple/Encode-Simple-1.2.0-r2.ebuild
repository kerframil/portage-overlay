# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=DBOOK
DIST_VERSION=1.002
inherit perl-module

DESCRIPTION="Encode and decode text, simply"
LICENSE="|| ( Artistic-2 GPL-1+ )"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test +xs"
RESTRICT="mirror"

RDEPEND="
	>=virtual/perl-Encode-2.850.0
	>=virtual/perl-Exporter-5.570.0
	xs? ( dev-perl/Unicode-UTF8 )
"

DEPEND="${RDEPEND}
	test? (
		dev-perl/Test-Needs
		dev-perl/Test-Without-Module
		>=virtual/perl-Test-Simple-0.960.0
	)
"
