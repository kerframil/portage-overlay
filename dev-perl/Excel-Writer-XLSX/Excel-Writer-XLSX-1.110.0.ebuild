# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR="JMCNAMARA"
DIST_VERSION="1.09"

inherit perl-module

DESCRIPTION="Create a new file in the Excel 2007+ XLSX format"

SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="minimal"
RESTRICT="mirror"

RDEPEND="
	>=dev-lang/perl-5.10
	>=dev-perl/Archive-Zip-1.300.0
	>=virtual/perl-File-Temp-0.190.0
	!minimal? (
		dev-perl/Date-Calc
		dev-perl/Date-Manip
	)
"
DEPEND="${RDEPEND}"
