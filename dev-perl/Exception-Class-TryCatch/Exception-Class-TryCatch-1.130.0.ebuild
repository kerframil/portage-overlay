# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=DAGOLDEN
DIST_VERSION=1.13
inherit perl-module

DESCRIPTION="Syntactic try/catch sugar for use with Exception::Class"
LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	dev-perl/Exception-Class
	virtual/perl-Exporter
	virtual/perl-File-Spec
	virtual/perl-Scalar-List-Utils
	virtual/perl-version
"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
"
