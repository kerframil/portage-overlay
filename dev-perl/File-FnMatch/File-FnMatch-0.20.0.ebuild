# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MJP
DIST_VERSION=0.02
inherit perl-module

DESCRIPTION="Simple filename and pathname matching"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

DEPEND="
	virtual/perl-ExtUtils-MakeMaker
	test? ( virtual/perl-Test )
"
