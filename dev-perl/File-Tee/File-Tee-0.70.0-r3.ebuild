# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=SALVA
DIST_VERSION=0.07
inherit perl-module

DESCRIPTION="Replicate data sent to a Perl stream"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
