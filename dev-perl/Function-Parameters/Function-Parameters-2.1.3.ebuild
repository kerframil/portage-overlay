# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MAUKE
DIST_VERSION=2.001003
inherit perl-module

DESCRIPTION="An implementation of subroutine signatures for functions and methods"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	>=dev-lang/perl-5.14
	dev-perl/Dir-Self
	virtual/perl-Carp
	virtual/perl-XSLoader
"
DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	virtual/perl-File-Spec
	test? ( dev-perl/Test-Fatal )
"
