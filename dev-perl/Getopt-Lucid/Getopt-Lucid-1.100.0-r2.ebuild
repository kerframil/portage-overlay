# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=DAGOLDEN
DIST_VERSION=1.10
inherit perl-module

DESCRIPTION="Clear, readable syntax for command line processing"
LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	>=dev-perl/Exception-Class-TryCatch-1.100.0
	>=virtual/perl-Storable-2.160.0
"
DEPEND="${RDEPEND}
	>=virtual/perl-ExtUtils-MakeMaker-6.170.0
	test? (
		>=virtual/perl-Test-Simple-0.620.0
	)
"
