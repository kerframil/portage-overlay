# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=DAGOLDEN
DIST_VERSION=1.09
inherit perl-module

DESCRIPTION="Clear, readable syntax for command line processing"
LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	dev-perl/Exception-Class
	dev-perl/Exception-Class-TryCatch
	virtual/perl-Carp
	virtual/perl-Data-Dumper
	virtual/perl-Exporter
	virtual/perl-File-Spec
	virtual/perl-Scalar-List-Utils
	virtual/perl-Storable
"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
"
