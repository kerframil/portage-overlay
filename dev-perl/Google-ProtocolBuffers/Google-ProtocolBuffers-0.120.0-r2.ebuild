# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=SAXJAZMAN
DIST_VERSION=0.12
inherit perl-module

DESCRIPTION="Simple interface to Google Protocol Buffers"
SRC_URI="mirror://cpan/authors/id/S/SA/SAXJAZMAN/protobuf/${PN}-${DIST_VERSION}.tar.gz"

SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"

RDEPEND="
	>=dev-lang/perl-5.12
	>=dev-perl/Parse-RecDescent-1.940.0
	dev-perl/Class-Accessor
"
DEPEND="${RDEPEND}"
