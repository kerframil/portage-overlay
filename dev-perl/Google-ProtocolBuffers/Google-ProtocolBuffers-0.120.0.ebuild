# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DIST_AUTHOR=SAXJAZMAN
DIST_VERSION=0.12
inherit perl-module

DESCRIPTION="Simple interface to Google Protocol Buffers"
SRC_URI="mirror://cpan/authors/id/S/SA/SAXJAZMAN/protobuf/${PN}-${DIST_VERSION}.tar.gz"

SLOT="0"
KEYWORDS="~amd64"
IUSE="test"

RDEPEND="
	dev-perl/Class-Accessor
	dev-perl/Parse-RecDescent
"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? (
		virtual/perl-Test-Simple
	)
"
