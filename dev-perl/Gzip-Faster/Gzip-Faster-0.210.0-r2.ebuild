# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR="BKB"
DIST_VERSION="0.21"
inherit perl-module

DESCRIPTION="Simple and fast gzip and gunzip"

SLOT="0"
KEYWORDS="amd64"
RESTRICT="mirror"

RDEPEND="
	app-arch/gzip
"
DEPEND="${RDEPEND}"

# TODO: Package Image::PNG::Libpng so that this test may run
PERL_RM_FILES=(
	t/png.t
)
