# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=KARAVELOV
DIST_VERSION=0.07
inherit perl-module

DESCRIPTION="Constant hash algorithm"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
