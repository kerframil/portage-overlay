# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=DMUEY
DIST_VERSION=0.2
inherit perl-module

DESCRIPTION="is_interactive() without large deps"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

BDEPEND="
	dev-perl/Module-Build
"
