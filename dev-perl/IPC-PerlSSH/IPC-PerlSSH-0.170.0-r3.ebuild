# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=PEVANS
DIST_VERSION=0.17
inherit perl-module

DESCRIPTION="Execute remote perl code over an SSH link"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	>=virtual/perl-Exporter-5.570.0
"
DEPEND="
	${RDEPEND}
"
BDEPEND="${RDEPEND}
	dev-perl/Module-Build
"
