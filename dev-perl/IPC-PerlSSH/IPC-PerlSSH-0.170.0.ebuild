# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=PEVANS
DIST_VERSION=0.17
inherit perl-module

DESCRIPTION="Execute remote perl code over an SSH link"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	virtual/perl-Exporter
	virtual/perl-File-Temp
"

DEPEND="${RDEPEND}
	dev-perl/Module-Build
	virtual/perl-ExtUtils-Install
	test? (
		dev-perl/Test-Fatal
		virtual/perl-Test-Simple
	)
"
