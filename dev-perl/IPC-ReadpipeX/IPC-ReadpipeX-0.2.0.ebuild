# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=DBOOK
DIST_VERSION=0.002
inherit perl-module

DESCRIPTION="IPC::ReadpipeX - List form of readpipe/qx/backticks for capturing output"
LICENSE="|| ( Artistic-2 GPL-1+ )"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	virtual/perl-Carp
	virtual/perl-Exporter"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? (
		dev-perl/Test-Warnings
		virtual/perl-File-Spec
		virtual/perl-Test-Simple
	)"
