# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=DBOOK
DIST_VERSION=0.003
inherit perl-module

DESCRIPTION="IPC::ReadpipeX - List form of readpipe/qx/backticks for capturing output"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"
RESTRICT="mirror"

RDEPEND="
	>=virtual/perl-Exporter-5.570.0
"
DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? (
		dev-perl/Test-Warnings
		>=virtual/perl-Test-Simple-0.880.0
	)
"
