# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=ESTRABD
DIST_VERSION=0.07
inherit perl-module

DESCRIPTION="Perl interface to libpuzzle"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	dev-libs/libpuzzle
"
DEPEND="${RDEPEND}"
