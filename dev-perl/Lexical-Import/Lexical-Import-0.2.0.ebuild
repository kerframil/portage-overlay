# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=ZEFRAM
DIST_VERSION=0.002
inherit perl-module

DESCRIPTION="Clean imports from package-exporting modules"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	dev-perl/Lexical-Var
	dev-perl/Module-Runtime
	dev-perl/Params-Classify
	virtual/perl-XSLoader
	virtual/perl-version
"
DEPEND="${RDEPEND}
	dev-perl/Module-Build
	>=virtual/perl-ExtUtils-CBuilder-0.150.0
	test? (
		virtual/perl-Test-Simple
	)
"
