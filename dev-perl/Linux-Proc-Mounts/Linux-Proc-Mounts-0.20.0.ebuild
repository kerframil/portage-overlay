# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=SALVA
DIST_VERSION=0.02
inherit perl-module

DESCRIPTION="Various utility functions"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="virtual/perl-ExtUtils-MakeMaker"

PATCHES=("${FILESDIR}/RT124886.patch")
