# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=JAW
DIST_VERSION=0.01
inherit perl-module

DESCRIPTION="Perl module for Common Lisp like formatting"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
LICENSE="jeff-weisberg"

BDEPEND="virtual/perl-ExtUtils-MakeMaker"
