# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=ZOFFIX
DIST_VERSION=1.003001
inherit perl-module

DESCRIPTION="Write lists in strings like a human would"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
