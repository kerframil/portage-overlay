# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR="SYOHEX"
DIST_VERSION=0.05
inherit perl-module

DESCRIPTION="XS implementation of List::UtilsBy"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	virtual/perl-Exporter
	virtual/perl-XSLoader
	virtual/perl-parent
"
DEPEND="${RDEPEND}
	dev-perl/Module-Build-XSUtil
	test? (
		virtual/perl-Test-Simple
	)
"
