# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=ATHOMASON
DIST_VERSION=1.02
inherit perl-module

DESCRIPTION="Syslog priority constants as defined in RFC3164"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
