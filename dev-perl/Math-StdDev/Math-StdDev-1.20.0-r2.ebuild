# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=CDRAKE
DIST_VERSION=1.02
inherit perl-module

DESCRIPTION="Pure-perl mean and variance computation supporting Welford's online algorithm"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
