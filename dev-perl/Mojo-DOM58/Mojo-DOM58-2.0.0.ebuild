# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=DBOOK
DIST_VERSION=2.000
inherit perl-module

DESCRIPTION="Minimalistic HTML/XML DOM parser with CSS selectors"
LICENSE="|| ( Artistic-2 GPL-1+ )"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	virtual/perl-Carp
	virtual/perl-Exporter
	virtual/perl-Scalar-List-Utils"

DEPEND="${DEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? (
		virtual/perl-Encode
		virtual/perl-File-Spec
		virtual/perl-JSON-PP )"
