# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=DBOOK
DIST_VERSION=3.001
inherit perl-module

DESCRIPTION="Minimalistic HTML/XML DOM parser with CSS selectors"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"
RESTRICT="mirror"

RDEPEND="
	>=virtual/perl-Exporter-5.570.0
"
DEPEND="${RDEPEND}
	test? (
		>=virtual/perl-Test-Simple-0.960.0
	)
"
