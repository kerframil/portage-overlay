# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=TOBYINK
DIST_VERSION=0.011
inherit perl-module

DESCRIPTION="Declare class attributes Moose-style… but without Moose"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	dev-perl/Exporter-Tiny
	dev-perl/Moo
	dev-perl/Role-Tiny
"
DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
"
