# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=BYTERAZOR
DIST_VERSION=0.1
inherit perl-module

DESCRIPTION="A client class for the ClamAV virus scanner daemon"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	dev-perl/Moose
	dev-perl/Throwable
"
DEPEND="${RDEPEND}"
