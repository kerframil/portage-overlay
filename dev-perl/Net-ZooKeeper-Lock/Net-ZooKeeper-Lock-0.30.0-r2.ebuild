# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DIST_AUTHOR=TADAM
DIST_VERSION=0.03
inherit perl-module

DESCRIPTION="Distributed locks via ZooKeeper"

SLOT="0"
KEYWORDS="~amd64"
IUSE="test"

PATCHES=(
	"${FILESDIR}/01-guard-destruction-by-phase.patch"
	"${FILESDIR}/02-guard-destruction-by-pid.patch"
)

RDEPEND="
	dev-perl/Devel-GlobalDestruction
	dev-perl/Net-ZooKeeper
	dev-perl/Params-Validate
"
DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? (
		dev-perl/Test-Class
		virtual/perl-Test-Simple
	)
"
