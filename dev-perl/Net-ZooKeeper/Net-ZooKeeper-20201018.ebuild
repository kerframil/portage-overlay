# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=MAF
DIST_VERSION=0.41
inherit perl-module

COMMIT_ID="66e1a360aff9c39af728c36092b540a4b6045f70"
SRC_URI="https://github.com/mark-5/p5-net-zookeeper/archive/${COMMIT_ID}.zip"
DESCRIPTION="Perl extension for Apache ZooKeeper"
LICENSE="Apache-2.0"

SLOT="0"
KEYWORDS="~amd64"
RESTRICT="mirror"

RDEPEND="
	dev-libs/zookeeper-c
"
DEPEND="${RDEPEND}"

S="${WORKDIR}/p5-net-zookeeper-${COMMIT_ID}"
