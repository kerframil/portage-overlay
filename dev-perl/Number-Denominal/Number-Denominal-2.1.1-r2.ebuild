# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=ZOFFIX
DIST_VERSION=2.001001
inherit perl-module

DESCRIPTION="Break up numbers into arbitrary denominations"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	>=dev-perl/List-ToHumanString-1.2.0
"
DEPEND="${RDEPEND}
	dev-perl/Test-Deep
	>=virtual/perl-ExtUtils-MakeMaker-6.300.0
"
