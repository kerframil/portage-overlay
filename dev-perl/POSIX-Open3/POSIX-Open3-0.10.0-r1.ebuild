# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR="AMBS"
DIST_VERSION="0.01"
DIST_SECTION="POSIX"
inherit perl-module

DESCRIPTION="Open a process for reading, writing, and error handling using open3()"

LICENSE="|| ( Artistic GPL-1+ )"
SLOT="0"
KEYWORDS=""
IUSE="test"
RESTRICT="mirror"

DEPEND="
	virtual/perl-ExtUtils-MakeMaker
	test? (
		virtual/perl-Test-Simple
	)
"

PERL_RM_FILES=(
	t/00-load.t
	manifest.t
	pod.t
)
