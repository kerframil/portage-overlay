# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_NAME=POSIX-2008
DIST_AUTHOR=CGPAN
DIST_VERSION=0.16
inherit perl-module

DESCRIPTION="Perl interface to POSIX.1-2008"
LICENSE="WTFPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="virtual/perl-ExtUtils-MakeMaker"
