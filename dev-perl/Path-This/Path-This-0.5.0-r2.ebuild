# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=DBOOK
DIST_VERSION=0.005
inherit perl-module

DESCRIPTION="Path to this source file or directory"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"
RESTRICT="mirror"

DEPEND="
	test? (
		>=virtual/perl-Test-Simple-0.880.0
	)
"
