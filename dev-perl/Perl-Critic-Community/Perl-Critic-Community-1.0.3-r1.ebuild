# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR="DBOOK"
DIST_VERSION="v1.0.3"
inherit perl-module

DESCRIPTION="Community-inspired Perl::Critic policies"

LICENSE="Artistic-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="
	mirror
	!test? ( test )
"

RDEPEND="
	>=dev-perl/PPI-1.222.0
	>=dev-perl/Path-Tiny-0.101.0
	>=dev-perl/Perl-Critic-1.126.0
	>=dev-perl/Perl-Critic-Policy-Variables-ProhibitLoopOnHash-0.5.0
	>=dev-perl/Perl-Critic-Pulp-90
	>=virtual/perl-Scalar-List-Utils-1.330.0
"
DEPEND="
	${RDEPEND}
"
BDEPEND="${RDEPEND}
	>=dev-perl/Module-Build-Tiny-0.34.0
"
