# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR="XSAWYERX"
DIST_VERSION="0.008"
inherit perl-module

DESCRIPTION="Don't write loops on hashes, only on keys and values of hashes"

SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	>=virtual/perl-Scalar-List-Utils-1.330.0
"
DEPEND="${RDEPEND}"
