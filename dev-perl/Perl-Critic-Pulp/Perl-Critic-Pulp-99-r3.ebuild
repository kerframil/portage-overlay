# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR="KRYDE"
DIST_VERSION="99"
inherit perl-module

DESCRIPTION="Some add-on policies for Perl::Critic"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	>=dev-perl/IO-String-1.20.0
	>=dev-perl/List-MoreUtils-0.240.0
	>=dev-perl/PPI-1.222.0
	>=dev-perl/Perl-Critic-1.84.0
	>=dev-perl/Pod-MinimumVersion-50
	dev-perl/Pod-Parser
"
DEPEND="${RDEPEND}"
