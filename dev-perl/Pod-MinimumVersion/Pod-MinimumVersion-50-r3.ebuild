# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR="KRYDE"
DIST_VERSION="50"
inherit perl-module

DESCRIPTION="Perl version for POD directives used"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	>=dev-perl/IO-String-1.20.0
"
DEPEND="${RDEPEND}"
