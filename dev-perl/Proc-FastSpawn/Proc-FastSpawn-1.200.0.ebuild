# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MLEHMANN
DIST_VERSION=1.2
inherit perl-module

DESCRIPTION="fork+exec, or spawn, a subprocess as quickly as possible"
LICENSE="all-rights-reserved"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="
	virtual/perl-ExtUtils-MakeMaker
"
