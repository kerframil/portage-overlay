# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MAUKE
DIST_VERSION=1.0102
inherit perl-module

DESCRIPTION="Various utility functions"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	>=dev-lang/perl-5.14
	virtual/perl-Carp
	virtual/perl-File-Spec
	virtual/perl-XSLoader
	virtual/perl-if
"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
"
