# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=MSCHILLI
DIST_VERSION=0.36
inherit perl-module

DESCRIPTION="Object-oriented interface to RRDTool"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

PATCHES=(
	"${FILESDIR}/explicitly-fail-if-rrdtool-is-unavailable.patch"
)

RDEPEND="
	>=dev-perl/Log-Log4perl-0.400.0
	net-analyzer/rrdtool
"
DEPEND="${RDEPEND}"
