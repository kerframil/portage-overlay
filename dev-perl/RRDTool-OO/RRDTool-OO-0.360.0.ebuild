# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MSCHILLI
DIST_VERSION=0.36
inherit perl-module

DESCRIPTION="Object-oriented interface to RRDTool"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	dev-perl/Log-Log4perl
	net-analyzer/rrdtool
	virtual/perl-Carp
	virtual/perl-Data-Dumper
	virtual/perl-Storable"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	test? (
		virtual/perl-Test-Simple
	)"
