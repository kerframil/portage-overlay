# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=BADALEX
DIST_VERSION=0.03
inherit perl-module

DESCRIPTION="Replaces rand() with /dev/urandom"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

DEPEND="
	virtual/perl-ExtUtils-MakeMaker
	test? (
		virtual/perl-Test-Simple
	)
"
