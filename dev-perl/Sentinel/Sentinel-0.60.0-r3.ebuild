# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=PEVANS
DIST_VERSION=0.06
inherit perl-module

DESCRIPTION="Create lightweight SCALARs with get/set callbacks"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="test"
RESTRICT="
	mirror
	!test? ( test )
"

DEPEND="
	test? (
		dev-perl/Test-Refcount
		>=virtual/perl-Test-Simple-0.880.0
	)"
BDEPEND="
	>=dev-perl/Module-Build-0.400.400
"
