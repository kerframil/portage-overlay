# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=BDFOY
DIST_VERSION=2.008
inherit perl-module

DESCRIPTION="Work with the cross product of two or more sets"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

DEPEND="
	>=virtual/perl-ExtUtils-MakeMaker-6.640.0
"
