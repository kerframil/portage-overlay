# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=MORNINDED
DIST_VERSION=1.04
inherit perl-module

DESCRIPTION="Set operations for functional programming"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
