# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MORNINDED
DIST_VERSION=1.04
inherit perl-module

DESCRIPTION="Set operations for functional programming"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="virtual/perl-Exporter"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
"
