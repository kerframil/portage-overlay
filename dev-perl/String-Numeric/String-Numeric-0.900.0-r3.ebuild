# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=CHANSEN
DIST_VERSION=0.9
inherit perl-module

DESCRIPTION="Determine whether a string represents a numeric value"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"
RESTRICT="mirror"

DEPEND="
	dev-perl/Module-Install
	>=virtual/perl-ExtUtils-MakeMaker-6.420.0
	test? (
		dev-perl/Test-Exception
		>=virtual/perl-Test-Simple-0.860.0
	)
"
