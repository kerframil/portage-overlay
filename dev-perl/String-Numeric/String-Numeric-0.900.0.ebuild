# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=CHANSEN
DIST_VERSION=0.9
inherit perl-module

DESCRIPTION="Determine whether a string represents a numeric value"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

DEPEND="
	dev-perl/Module-Install
	virtual/perl-ExtUtils-MakeMaker
	test? (
		dev-perl/Test-Exception
		virtual/perl-Test-Simple
	)
"
