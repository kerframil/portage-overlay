# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=NEILB
DIST_VERSION=0.03
inherit perl-module

DESCRIPTION="Length or width of string excluding ANSI tty codes"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"
RESTRICT="mirror"

RDEPEND="
	>=dev-perl/Ref-Util-0.202.0
	>=dev-perl/String-TtyLength-0.20.0
	>=dev-perl/Unicode-EastAsianWidth-12.0.0
"
DEPEND="${DEPEND}
	test? (
		dev-perl/Test2-Suite
	)
"
