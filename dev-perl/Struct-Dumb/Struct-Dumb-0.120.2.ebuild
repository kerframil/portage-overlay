# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DIST_AUTHOR=PEVANS
DIST_VERSION=0.12
inherit perl-module

DESCRIPTION="Make simple lightweight record-like structures"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

DEPEND="
	>=dev-perl/Module-Build-0.400.400
	test? (
		dev-perl/Test-Fatal
		virtual/perl-Test-Simple
	)
"
