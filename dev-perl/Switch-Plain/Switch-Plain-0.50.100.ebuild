# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MAUKE
DIST_VERSION=0.0501
inherit perl-module

DESCRIPTION="A simple switch statement for Perl"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	>=dev-lang/perl-5.14
	virtual/perl-Carp
	virtual/perl-XSLoader"

DEPEND="${RDEPEND}
	virtual/perl-ExtUtils-MakeMaker
	virtual/perl-File-Spec
	test? (
		virtual/perl-Test-Simple
	)"
