# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=PEVANS
DIST_VERSION=0.18
inherit perl-module

DESCRIPTION="A try/catch/finally syntax, implemented as a native keyword"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	>=dev-lang/perl-5.14
	virtual/perl-Carp
	virtual/perl-XSLoader
"

DEPEND="${RDEPEND}
	dev-perl/Module-Build
"
