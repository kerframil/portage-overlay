# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=FELIPE
DIST_VERSION=0.04
inherit perl-module

DESCRIPTION="A fix for Perl’s system call character encoding"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"
RESTRICT="mirror"

RDEPEND="
	>=dev-lang/perl-5.14
"
DEPEND="
	test? (
		dev-perl/Test-FailWarnings
	)
"
