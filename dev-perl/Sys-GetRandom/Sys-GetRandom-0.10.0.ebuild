# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8
DIST_AUTHOR=MAUKE
DIST_VERSION=0.01
inherit perl-module

DESCRIPTION="Perl interface to getrandom(2)"

SLOT="0"
KEYWORDS="~arm64 amd64 ~x86"
RESTRICT="mirror"

DEPEND=">=virtual/perl-ExtUtils-MakeMaker-6.700.0"
