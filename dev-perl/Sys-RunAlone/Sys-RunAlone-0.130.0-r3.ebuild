# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=TILLY
DIST_VERSION=0.13
inherit perl-module

DESCRIPTION="Make sure only one invocation of a script is active at a time"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
