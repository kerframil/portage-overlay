# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=OALDERS
DIST_VERSION=0.16
inherit perl-module

DESCRIPTION="A minimal, pure perl TOML parser and serializer"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~arm64 ~hppa ~loong ~m68k ~mips ~ppc ~ppc64 ~riscv ~s390 ~sparc ~x86 ~x64-cygwin ~amd64-linux ~x86-linux ~ppc-macos ~x64-macos ~sparc-solaris ~sparc64-solaris ~x64-solaris ~x86-solaris"
IUSE="minimal test"
RESTRICT="
	mirror
	!test? ( test )
"

RDEPEND="
	>=dev-lang/perl-5.18
	>=virtual/perl-Math-BigInt-1.999.718
	!minimal? (
		dev-perl/Types-Serialiser
	)
"

DEPEND="${RDEPEND}
	test? (
		dev-perl/DateTime-Format-ISO8601
		dev-perl/DateTime-Format-RFC3339
		dev-perl/Test-Pod
		dev-perl/Test2-Suite
	)
"

PERL_RM_FILES=(
	# Requires TOML::Parser
	t/parity.t
)
