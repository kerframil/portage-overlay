# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=KARUPA
DIST_VERSION=0.97
inherit perl-module

DESCRIPTION="Parser for Tom's Obvious, Minimal Language"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	dev-perl/TOML-Parser
	dev-perl/Types-Serialiser
	virtual/perl-Carp
	virtual/perl-Encode
	virtual/perl-Exporter
	virtual/perl-parent"

DEPEND="${RDEPEND}
	dev-perl/Module-Build-Tiny
	test? (
		dev-perl/Test-Deep
		virtual/perl-MIME-Base64
		virtual/perl-Storable
		virtual/perl-Test-Simple
	)"
