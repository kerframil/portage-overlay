# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=ZENTOOO
DIST_VERSION=0.10
inherit perl-module

DESCRIPTION="Just feel easy to use colors on your small scripts"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

RDEPEND="
	dev-perl/autobox
"

DEPEND="${RDEPEND}
	>=virtual/perl-ExtUtils-MakeMaker-6.420.0
"
