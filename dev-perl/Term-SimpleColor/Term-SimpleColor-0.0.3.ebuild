# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=UNLEARNED
DIST_VERSION=v${PV}
inherit perl-module

DESCRIPTION="A very simple color screen output"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	virtual/perl-Carp
	virtual/perl-Exporter"

DEPEND="${RDEPEND}
	>=dev-perl/Module-Build-0.360.0
	test? ( virtual/perl-Test-Simple )
"
