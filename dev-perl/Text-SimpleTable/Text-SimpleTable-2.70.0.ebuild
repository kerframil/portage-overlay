# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=MRAMBERG
DIST_VERSION=2.07
inherit perl-module

DESCRIPTION="Simple eye-candy ASCII tables"
SLOT="0"
KEYWORDS="amd64 ~x86"

DEPEND="virtual/perl-ExtUtils-MakeMaker"
