# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=NEILB
DIST_VERSION=0.05
inherit perl-module

DESCRIPTION="Simple text tables from 2D arrays, with limited templating options"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	virtual/perl-Carp
	virtual/perl-Exporter
	virtual/perl-parent
	virtual/perl-Scalar-List-Utils"

DEPEND="${DEPEND}
	test? ( dev-perl/Test-Fatal )
	virtual/perl-ExtUtils-MakeMaker"
