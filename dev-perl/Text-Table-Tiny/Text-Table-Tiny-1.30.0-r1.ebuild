# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=NEILB
DIST_VERSION=1.03
inherit perl-module

DESCRIPTION="Simple text tables from 2D arrays, with limited templating options"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"
RESTRICT="
	mirror
	!test? ( test )
"

RDEPEND="
	>=dev-perl/Ref-Util-0.202.0
	>=dev-perl/String-TtyLength-0.20.0
"
DEPEND="${DEPEND}
	test? (
		dev-perl/Test-Fatal
	)
"
