# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=ROODE
DIST_VERSION=0.01
inherit perl-module

DESCRIPTION="Select a string variant based on a quantity"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"
