# Copyright 2023 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=DWHEELER
DIST_VERSION=0.12
inherit perl-module

DESCRIPTION="ISO 8601 Subclass of Time::Piece"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="mirror"

DEPEND="
	test? (
		>=dev-perl/Test-Pod-1.410.0
		>=virtual/perl-Test-Simple-0.170.0
	)
"
BDEPEND="${RDEPEND}
	>=dev-perl/Module-Build-0.270.100
"

