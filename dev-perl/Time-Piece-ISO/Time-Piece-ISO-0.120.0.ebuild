# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=DWHEELER
DIST_VERSION=0.12
inherit perl-module

DESCRIPTION="ISO 8601 Subclass of Time::Piece"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="virtual/perl-Time-Piece"

DEPEND="${RDEPEND}
	dev-perl/Module-Build
	virtual/perl-ExtUtils-Install
"
