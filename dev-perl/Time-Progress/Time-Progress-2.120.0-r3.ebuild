# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DIST_AUTHOR=CADE
DIST_VERSION=2.12
inherit perl-module

DESCRIPTION="Elapsed and estimated finish time reporting"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"
RESTRICT="mirror"

DEPEND="
	test? (
		>=virtual/perl-Test-Simple-0.880.0
	)
"
