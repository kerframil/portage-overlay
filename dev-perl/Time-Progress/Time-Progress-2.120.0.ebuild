# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DIST_AUTHOR=CADE
DIST_VERSION=2.12
inherit perl-module

DESCRIPTION="Elapsed and estimated finish time reporting"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	virtual/perl-Carp
"

DEPEND="${RDEPEND}
	test? ( virtual/perl-Test-Simple )
"
