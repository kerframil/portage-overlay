# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DIST_AUTHOR=LEONT
DIST_VERSION=0.03
inherit perl-module

DESCRIPTION="Lazy variables"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE="test"

RDEPEND="
	dev-perl/B-Hooks-EndOfScope
	dev-perl/Devel-Declare
	dev-perl/namespace-clean
	>=virtual/perl-Exporter-5.570.0
"
DEPEND="${RDEPEND}
	dev-perl/Module-Build
	virtual/perl-ExtUtils-CBuilder
	test? (
		virtual/perl-Test-Simple
	)
"
