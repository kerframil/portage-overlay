# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

S="${WORKDIR}/${P}/php/${PN}"

PHP_EXT_NAME="${PN}"
PHP_EXT_SAPIS="apache2 cgi cli fpm"
USE_PHP="php7-1 php7-2"
PHP_EXT_ECONF_ARGS="--with-libpuzzle"

inherit git-r3 php-ext-source-r3

DESCRIPTION="A library designed to quickly find visually similar images"
HOMEPAGE="https://libpuzzle.pureftpd.org/project/libpuzzle"
EGIT_REPO_URI="https://github.com/jedisct1/libpuzzle"
EGIT_COMMIT="a1023d7"

LICENSE="ICL"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="dev-libs/libpuzzle"
RDEPEND="${DEPEND}"

DOCS=()

# the default function finishes by calling eautoreconf and failing
php-ext-source-r3_phpize() {
	autotools_run_tool "${PHPIZE}"
}

src_prepare() {
	eapply -p3 "${FILESDIR}/${PN}-php7-compat.patch"
	php-ext-source-r3_src_prepare
}

src_configure() {
	php-ext-source-r3_src_configure
}

src_install() {
	php-ext-source-r3_src_install
}
