# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PHP_EXT_NAME="decimal"
PHP_EXT_INI="yes"
PHP_EXT_ZENDEXT="no"
USE_PHP="php7-4 php8-0 php8-1 php8-2 php8-3"

inherit php-ext-pecl-r3

DESCRIPTION="Correctly-rounded, arbitrary precision decimal floating-point arithmetic"
HOMEPAGE="https://php-decimal.github.io/"
RESTRICT="nomirror"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64"

DEPEND=">=dev-libs/mpdecimal-2.4:="
RDEPEND="${DEPEND}"
