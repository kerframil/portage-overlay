# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit eutils

DESCRIPTION="BPG (Better Portable Graphics) is a new image format"
HOMEPAGE="http://bellard.org/bpg/"
SRC_URI="http://bellard.org/bpg/${P}.tar.gz"
LICENSE="LGPL-2.1 BSD"
SLOT="0"
KEYWORDS="amd64"
IUSE="jctvc sdl +x265"

REQUIRED_USE="|| ( jctvc x265 )"

DEPEND="
	dev-lang/yasm
	dev-util/cmake"

RDEPEND="
	media-libs/libjpeg-turbo
	media-libs/libpng:0
	sys-process/numactl
	sdl? ( media-libs/sdl-image )
	x265? ( media-libs/x265:= )"

PATCHES=(
	"${FILESDIR}"/${PN}-0.9.6-numa_linking.patch
	"${FILESDIR}"/${PN}-cxx98.patch
)

src_prepare() {
	default

	sed \
		-e "/^CFLAGS:=/ s/-Os/${CFLAGS}/" \
		-e "/^LDFLAGS=/ s/-g/${LDFLAGS}/" \
		-e '/^USE_/ s/^/#/' \
		-i Makefile || die
}

src_compile() {
	local -a args
	use jctvc && args+=("USE_JCTVC=y")
	use sdl   && args+=("USE_BPGVIEW=y")
	use x265  && args+=("USE_X265=y")
	emake "${args[@]}"
}

src_install() {
	dobin bpgdec bpgenc
	use sdl && dobin bpgview
	dolib.a libbpg.a
	doheader {bpgenc,libbpg}.h
	dodoc ChangeLog README doc/bpg_spec.txt
}
