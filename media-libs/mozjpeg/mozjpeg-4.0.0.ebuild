# Copyright 2018-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake

DESCRIPTION="Improved JPEG encoder based on libjpeg-turbo"
HOMEPAGE="https://github.com/mozilla/mozjpeg"
SRC_URI="https://github.com/mozilla/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD IJG"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="png"

DEPEND=">=dev-lang/nasm-2.01"
RDEPEND="
	png? ( media-libs/libpng:0= )
	sys-libs/zlib"

src_configure() {
	mycmakeargs=(
		-DCMAKE_INSTALL_LIBDIR="/usr/$(get_libdir)/${PN}"
		-DCMAKE_INSTALL_INCLUDEDIR="/usr/include/${PN}"
		-DENABLE_STATIC=FALSE
		-DPNG_SUPPORTED=$(usex png)
	)
	cmake_src_configure
}

src_install() {
	local dir f
	cmake_src_install
	for dir in "${D}"/usr/{bin,share/man/man1}; do
		cd "$dir" || die
		for f in *; do
				mv {,moz}"$f" || die
		done
	done
}
