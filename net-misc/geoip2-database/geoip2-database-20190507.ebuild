# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="GeoLite2 IP geolocation databases compiled by MaxMind"
HOMEPAGE="https://dev.maxmind.com/geoip/geoip2/geolite2/"
SRC_URI="
	https://geolite.maxmind.com/download/geoip/database/GeoLite2-City.tar.gz -> GeoLite2-City-${PV}.tar.gz
	https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country.tar.gz -> GeoLite2-Country-${PV}.tar.gz
	https://geolite.maxmind.com/download/geoip/database/GeoLite2-ASN.tar.gz -> GeoLite2-ASN-${PV}.tar.gz"

LICENSE="CC-BY-SA-4.0"
SLOT="0"
KEYWORDS="amd64 ~x86"

S=${WORKDIR}

src_unpack() {
	local db
	for db in GeoLite2-{City,Country,ASN}; do
		tar --strip-components=1 --wildcards -xf \
			"${DISTDIR}/${db}-${PV}.tar.gz" "*/${db}.mmdb"
	done
	tar --strip-components=1 --wildcards -xf \
		"${DISTDIR}/GeoLite2-ASN-${PV}.tar.gz" "*/COPYRIGHT.txt" "*/LICENSE.txt"
}

src_install() {
	dodoc {COPYRIGHT,LICENSE}.txt
	insinto /usr/share/GeoIP
	doins GeoLite2-{City,Country,ASN}.mmdb
}
