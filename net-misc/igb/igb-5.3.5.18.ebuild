EAPI=6

inherit linux-mod

DESCRIPTION="Linux igb* Base Driver for Gigabit Intel® Network Connections"
HOMEPAGE="http://www.intel.com/support/network/adapter/pro100/sb/CS-032498.htm"
SRC_URI="mirror://sourceforge/project/e1000/${PN}%20stable/${PV}/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
IUSE=""

KEYWORDS="~amd64 ~ia64 ~x86"

MODULE_NAMES="igb(updates/drivers/net/ethernet/intel:${S}/src)"
BUILD_TARGETS="install"

CONFIG_CHECK="~!IGB"
ERROR_IGB="Your kernel already has CONFIG_IGB enabled. You will need to either \
unload the existing module or reboot, in order to use the driver provided by $P"

src_compile() {
	cd "${S}/src"
	emake BUILD_KERNEL=${KV_FULL} ARCH="$(tc-arch-kernel)"
}

src_install() {
	linux-mod_src_install
	dodoc README
	doman igb.7
}
