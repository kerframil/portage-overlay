# Copyright 2020 Kerin Millar <kfm@plushkava.net>
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 autotools systemd

DESCRIPTION="nftables load balancer"
HOMEPAGE="https://www.zevenet.com/knowledge-base/nftlb/what-is-nftlb/"
EGIT_REPO_URI="https://github.com/zevenet/nftlb"
EGIT_COMMIT="4cae439"

LICENSE="AGPL-3"
SLOT="0"
KEYWORDS="amd64 ~x86"
RESTRICT="test"

DEPEND="
	>=dev-libs/jansson-2.3:=
	dev-libs/libev:=
	>=net-firewall/nftables-0.9:=[modern-kernel]
	>=net-libs/libmnl-1.0.4:0="

RDEPEND="${DEPEND}
	app-misc/jshon
	net-misc/curl"

src_prepare() {
	default
	eautoreconf
}

src_install() {
	local f

	default

	newinitd "${FILESDIR}"/nftlb.initd-r7 nftlb
	newconfd "${FILESDIR}"/nftlb.confd nftlb
	fperms 640 /etc/conf.d/nftlb
	keepdir /var/lib/nftlb
	systemd_dounit "${FILESDIR}"/nftlb.service

	doman "${FILESDIR}"/nftlb.8

	docinto samples
	cd tests || die
	for f in */input.json; do
		dodoc -r "${f%/*}"
	done
}

pkg_postinst() {
	(
		umask 177
		set -C
		path=/var/lib/nftlb/config.json
		if 2>/dev/null echo '{ "farms": [] }' >"$path"; then
			elog "An empty configuration file has been installed as $path."
		fi
	)
	elog "Configuration samples can be found at /usr/share/doc/nftlb-${PVR}/samples."
	elog "Be sure to review /etc/conf.d/nftlb before first starting."
}
