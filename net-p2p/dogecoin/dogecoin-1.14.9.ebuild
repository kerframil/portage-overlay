# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

WANT_AUTOCONF="2.5"
inherit autotools flag-o-matic systemd tmpfiles

DESCRIPTION="An open-source peer-to-peer digital currency, favoured by Shiba Inus worldwide"
HOMEPAGE="https://github.com/dogecoin"
SRC_URI="https://github.com/${PN}/${PN}/archive/v${PV}.tar.gz -> ${PN}-v${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
IUSE="cpu_flags_x86_sse2 experimental wallet zmq"
REQUIRED_USE="cpu_flags_x86_sse2? ( experimental )"

DEPEND="
	dev-build/automake:=
	dev-build/libtool
	>=dev-libs/boost-1.84.0
	dev-libs/libevent:=
	dev-libs/openssl
	wallet? ( sys-libs/db:5.3=[cxx] )
	zmq? ( net-libs/cppzmq )
"

RDEPEND="${DEPEND}
	acct-user/dogecoin
	!net-p2p/dogecoin-qt
"

PATCHES=( "${FILESDIR}"/hardened-all.patch )

src_prepare() {
	default

	einfo "Generating autotools files..."
	eaclocal -I "${S}"
	eautoreconf
}

src_configure() {
	local my_econf=(
		--disable-bench
		--disable-dependency-tracking
		--disable-tests
		--enable-c++14
		--without-gui
		--without-miniupnpc
		$(use_enable cpu_flags_x86_sse2 scrypt-sse2)
		$(use_enable experimental experimental)
		$(use_enable wallet)
		$(use_enable zmq)
	)

	econf "${my_econf[@]}"
}

src_install() {
	emake DESTDIR="${D}" install

	newtmpfiles "${FILESDIR}"/dogecoin.tmpfiles dogecoin.conf
	systemd_dounit contrib/init/dogecoind.service

	find "${ED}" -type f -name '*.la' -delete || die
}

pkg_postinst() {
	tmpfiles_process dogecoin.conf
}
