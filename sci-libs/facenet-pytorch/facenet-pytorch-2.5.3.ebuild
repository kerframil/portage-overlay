# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_SINGLE_IMPL=1
DISTUTILS_USE_PEP517=setuptools
DISTUTILS_EXT=1
inherit distutils-r1

DESCRIPTION="Pretrained Pytorch face detection and facial recognition models"
HOMEPAGE="https://github.com/timesler/facenet-pytorch"
SRC_URI="https://github.com/timesler/facenet-pytorch/archive/v${PV}.tar.gz -> ${P}.tar.gz"
RESTRICT="mirror"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	$(python_gen_cond_dep '
		>=dev-python/numpy-1.24[${PYTHON_USEDEP}]
		>=dev-python/pillow-10.2.0[${PYTHON_USEDEP}]
		>=dev-python/requests-2.0.0[${PYTHON_USEDEP}]
		>=dev-python/tqdm-4.0.0[${PYTHON_USEDEP}]
	')
	>=sci-libs/pytorch-2.2.0[${PYTHON_SINGLE_USEDEP}]
"
DEPEND="${RDEPEND}"
