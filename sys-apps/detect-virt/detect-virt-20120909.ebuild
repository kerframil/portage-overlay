# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs

DESCRIPTION="Simple yes/no virtualization detection"
HOMEPAGE="https://github.com/falconindy/bin-scripts"
SRC_URI="https://raw.githubusercontent.com/falconindy/bin-scripts/119cb0d/detect-virt.c"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="amd64 ~x86"

S=${WORKDIR}

src_unpack() { true; }

src_compile() {
	"$(tc-getCC)" -o detect-virt "${DISTDIR}"/detect-virt.c
}

src_install() {
	dobin detect-virt
}
