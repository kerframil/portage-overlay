# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

inherit git-r3 eutils fcaps

DESCRIPTION="Small linux tools to manage processes"
HOMEPAGE="https://github.com/izabera/pm-tools"
EGIT_REPO_URI="https://github.com/izabera/pm-tools.git"
EGIT_COMMIT="6062c77"

LICENSE="Unlicense"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DOCS=(README.md)

src_prepare() {
	sed -i -e 's/CFLAGS= -O2/CFLAGS +=/' -e '/sudo setcap/d' Makefile
	eapply_user
}

src_install() {
	dobin nochildren nosudo ptracer subreaper waiter
	einstalldocs
}

pkg_postinst() {
	fcaps cap_sys_ptrace /usr/bin/waiter
}
