# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

MY_P="${P/resource-}"
inherit git-r3 autotools eutils multilib

DESCRIPTION="Resources pack for Heartbeat / Pacemaker"
HOMEPAGE="http://www.linux-ha.org/wiki/Resource_Agents"
EGIT_REPO_URI="https://github.com/kerframil/resource-agents"
EGIT_BRANCH="improvements"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~hppa ~x86"
IUSE="doc libnet minimal rgmanager"

# USE=minimal prevents the use of certain optional features in ldirectord
RDEPEND="
	sys-apps/iproute2
	sys-cluster/cluster-glue
	!<sys-cluster/heartbeat-3.0
	libnet? ( net-libs/libnet:1.1 )

	dev-perl/libwww-perl
	dev-perl/MailTools
	virtual/perl-Digest-MD5
	!minimal? (
		libnet? ( virtual/perl-libnet )
		dev-perl/Authen-Radius
		dev-perl/DBI
		dev-perl/Digest-HMAC
		dev-perl/Mail-POP3Client
		dev-perl/Net-DNS
		dev-perl/Net-IMAP-Simple
		dev-perl/Net-IMAP-Simple-SSL
		dev-perl/Net-SSLeay
		dev-perl/perl-ldap
		virtual/logger
	)"
DEPEND="${RDEPEND}
	doc? (
		dev-libs/libxslt
		app-text/docbook-xsl-stylesheets
	)"
PATCHES=(
	"${FILESDIR}/4.2.0-configure.patch"
)

src_prepare() {
	default
	eautoreconf
}

src_configure() {
	econf \
		--disable-dependency-tracking \
		--disable-fatal-warnings \
		--localstatedir=/var \
		--docdir=/usr/share/doc/${PF} \
		--libdir=/usr/$(get_libdir) \
		--with-ocf-root=/usr/$(get_libdir)/ocf \
		--with-systemdsystemunitdir=no \
		--with-systemdtmpfilesdir=no \
		$(use_enable doc) \
		$(use_enable libnet)
}

src_install() {
	default
	rm -rf \
		"${D}"/etc/init.d/ldirectord \
		"${D}"/var/run/ \
		"${D}"/usr/$(get_libdir)/ocf/resource.d/redhat || die
	if ! use rgmanager; then
		rm -rf "${D}"/usr/share/cluster/ "${D}"/var/ || die
	fi
	newinitd "${FILESDIR}"/ldirectord.initd ldirectord
}

pkg_postinst() {
	elog "To use Resource Agents installed in /usr/$(get_libdir)/ocf/resource.d"
	elog "you have to emerge required runtime dependencies manually."
	elog ""
	elog "Description and dependencies of all Agents can be found on"
	elog "http://www.linux-ha.org/wiki/Resource_Agents"
	elog "or in the documentation of this package."
}
