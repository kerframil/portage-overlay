From 818ac39f70052b7cc2aa837172c25f665087fbfb Mon Sep 17 00:00:00 2001
From: Kerin Millar <kfm@plushkava.net>
Date: Tue, 10 Dec 2024 05:27:07 +0200
Subject: [PATCH] Port some of Gentoo's Kconfig options to 6.12-hardened

Incorporate the following Kconfig options from Gentoo's patchset.

- GENTOO_LINUX
- GENTOO_LINUX_UDEV
- GENTOO_LINUX_PORTAGE
- GENTOO_LINUX_INIT_SCRIPT
- GENTOO_LINUX_INIT_SYSTEMD
- GENTOO_PRINT_FIRMWARE_INFO

Signed-off-by: Kerin Millar <kfm@plushkava.net>
---
 Kconfig        |   2 +
 distro/Kconfig | 172 +++++++++++++++++++++++++++++++++++++++++++++++++
 2 files changed, 174 insertions(+)
 create mode 100644 distro/Kconfig

diff --git a/Kconfig b/Kconfig
index 745bc773f..e306bacea 100644
--- a/Kconfig
+++ b/Kconfig
@@ -30,3 +30,5 @@ source "lib/Kconfig"
 source "lib/Kconfig.debug"
 
 source "Documentation/Kconfig"
+
+source "distro/Kconfig"
diff --git a/distro/Kconfig b/distro/Kconfig
new file mode 100644
index 000000000..6401b8ef1
--- /dev/null
+++ b/distro/Kconfig
@@ -0,0 +1,172 @@
+menu "Gentoo Linux"
+
+config GENTOO_LINUX
+	bool "Gentoo Linux support"
+
+	default y
+
+	select CPU_FREQ_DEFAULT_GOV_SCHEDUTIL
+
+	help
+		In order to boot Gentoo Linux a minimal set of config settings needs to
+		be enabled in the kernel; to avoid the users from having to enable them
+		manually as part of a Gentoo Linux installation or a new clean config,
+		we enable these config settings by default for convenience.
+
+		See the settings that become available for more details and fine-tuning.
+
+config GENTOO_LINUX_UDEV
+	bool "Linux dynamic and persistent device naming (userspace devfs) support"
+
+	depends on GENTOO_LINUX
+	default y if GENTOO_LINUX
+
+	select DEVTMPFS
+	select TMPFS
+	select UNIX
+
+	select MMU
+	select SHMEM
+
+	help
+		In order to boot Gentoo Linux a minimal set of config settings needs to
+		be enabled in the kernel; to avoid the users from having to enable them
+		manually as part of a Gentoo Linux installation or a new clean config,
+		we enable these config settings by default for convenience.
+
+		Currently this only selects TMPFS, DEVTMPFS and their dependencies.
+		TMPFS is enabled to maintain a tmpfs file system at /dev/shm, /run and
+		/sys/fs/cgroup; DEVTMPFS to maintain a devtmpfs file system at /dev.
+
+		Some of these are critical files that need to be available early in the
+		boot process; if not available, it causes sysfs and udev to malfunction.
+
+		To ensure Gentoo Linux boots, it is best to leave this setting enabled;
+		if you run a custom setup, you could consider whether to disable this.
+
+config GENTOO_LINUX_PORTAGE
+	bool "Select options required by Portage features"
+
+	depends on GENTOO_LINUX
+	default y if GENTOO_LINUX
+
+	select CGROUPS
+	select NAMESPACES
+	select IPC_NS
+	select NET_NS
+	select PID_NS
+	select SYSVIPC
+	select USER_NS
+	select UTS_NS
+
+	help
+		This enables options required by various Portage FEATURES.
+		Currently this selects:
+
+		CGROUPS (required for FEATURES=cgroup)
+		IPC_NS  (required for FEATURES=ipc-sandbox)
+		NET_NS  (required for FEATURES=network-sandbox)
+		PID_NS  (required for FEATURES=pid-sandbox)
+		SYSVIPC (required by IPC_NS)
+
+		It is highly recommended that you leave this enabled as these FEATURES
+		are, or will soon be, enabled by default.
+
+menu "Support for init systems, system and service managers"
+	visible if GENTOO_LINUX
+
+config GENTOO_LINUX_INIT_SCRIPT
+	bool "OpenRC, runit and other script based systems and managers"
+
+	default y if GENTOO_LINUX
+
+	depends on GENTOO_LINUX
+
+	select BINFMT_SCRIPT
+	select CGROUPS
+	select EPOLL
+	select FILE_LOCKING
+	select INOTIFY_USER
+	select SIGNALFD
+	select TIMERFD
+
+	help
+		The init system is the first thing that loads after the kernel booted.
+
+		These config settings allow you to select which init systems to support;
+		instead of having to select all the individual settings all over the
+		place, these settings allows you to select all the settings at once.
+
+		This particular setting enables all the known requirements for OpenRC,
+		runit and similar script based systems and managers.
+
+		If you are unsure about this, it is best to leave this setting enabled.
+
+config GENTOO_LINUX_INIT_SYSTEMD
+	bool "systemd"
+
+	default n
+
+	depends on GENTOO_LINUX && GENTOO_LINUX_UDEV
+
+	select AUTOFS_FS
+	select BLK_DEV_BSG if SCSI
+	select BPF_SYSCALL
+	select CGROUP_BPF
+	select CGROUPS
+	select CRYPTO_HMAC
+	select CRYPTO_SHA256
+	select CRYPTO_USER_API_HASH
+	select DEVPTS_MULTIPLE_INSTANCES
+	select DMIID if X86_32 || X86_64 || X86
+	select EPOLL
+	select FANOTIFY
+	select FHANDLE
+	select FILE_LOCKING
+	select INOTIFY_USER
+	select IPV6
+	select KCMP
+	select NET
+	select NET_NS
+	select PROC_FS
+	select SECCOMP if HAVE_ARCH_SECCOMP
+	select SECCOMP_FILTER if HAVE_ARCH_SECCOMP_FILTER
+	select SIGNALFD
+	select SYSFS
+	select TIMERFD
+	select TMPFS_POSIX_ACL
+	select TMPFS_XATTR
+
+	select ANON_INODES
+	select BLOCK
+	select EVENTFD
+	select FSNOTIFY
+	select INET
+	select NLATTR
+
+	help
+		The init system is the first thing that loads after the kernel booted.
+
+		These config settings allow you to select which init systems to support;
+		instead of having to select all the individual settings all over the
+		place, these settings allows you to select all the settings at once.
+
+		This particular setting enables all the known requirements for systemd;
+		it also enables suggested optional settings, as the package suggests to.
+
+endmenu
+
+config GENTOO_PRINT_FIRMWARE_INFO
+	bool "Print firmware information that the kernel attempts to load"
+
+	depends on GENTOO_LINUX
+	default y
+
+	help
+		Enable this option to print information about firmware that the kernel
+		is attempting to load.  This information can be accessible via the
+		dmesg command-line utility
+
+		See the settings that become available for more details and fine-tuning.
+
+endmenu
-- 
2.45.2

