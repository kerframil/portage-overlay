# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

KERNEL_IUSE_GENERIC_UKI=
KERNEL_IUSE_MODULES_SIGN=1

inherit kernel-build toolchain-funcs verify-sig

UPSTREAM_PVR="${PV}-hardened1"
KV_LOCALVERSION="-hardened"
if [[ $PR != r0 ]]; then
	KV_LOCALVERSION+="-${PR}"
fi

DESCRIPTION="Linux kernel with features supplementing the upstream KSPP changes"
HOMEPAGE="https://github.com/anthraxx/linux-hardened"
MY_PATCH_URI="https://github.com/anthraxx/linux-hardened/releases/download/v${UPSTREAM_PVR}/linux-hardened-v${UPSTREAM_PVR}.patch"
SRC_URI+="
	https://cdn.kernel.org/pub/linux/kernel/v$(ver_cut 1).x/linux-$(ver_cut 1-2).tar.xz
	https://cdn.kernel.org/pub/linux/kernel/v$(ver_cut 1).x/patch-${PV}.xz
	${MY_PATCH_URI}
	verify-sig? ( ${MY_PATCH_URI}.sig )
"

S="${WORKDIR}/linux-${PV}${KV_LOCALVERSION}"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~arm ~arm64 ~hppa ~loong ~ppc ~ppc64 ~riscv ~sparc ~x86"
IUSE="debug"
REQUIRED_USE="savedconfig"

BDEPEND="
	sys-apps/ed
	verify-sig? ( sec-keys/openpgp-keys-anthraxx )
"
RDEPEND="!sys-kernel/gentoo-kernel:${SLOT}[hardened]"
PDEPEND=">=virtual/dist-kernel-${PV}"

QA_FLAGS_IGNORED="
	usr/src/linux-[^/]+/scripts/gcc-plugins/.*\.so
	usr/src/linux-[^/]+/vmlinux
	usr/src/linux-[^/]+/arch/powerpc/kernel/vdso[^/]*/vdso[^/]*\.so\.dbg
"

VERIFY_SIG_OPENPGP_KEY_PATH=${BROOT}/usr/share/openpgp-keys/anthraxx.asc

pkg_pretend() {
	if ! tc-is-gcc; then
		die "Owing to the use of GCC_PLUGINS, only gcc is supported for now."
	fi
	kernel-install_pkg_pretend
}

src_unpack() {
	if use verify-sig; then
		verify-sig_verify_detached "${DISTDIR}"/linux-hardened-v${UPSTREAM_PVR}.patch{,.sig}
	fi
	default
	mv "linux-$(ver_cut 1-2)" "linux-${PV}${KV_LOCALVERSION}" || die
}

src_prepare() {
	_apply_patches
	eapply_user
	restore_config .config
}

src_configure() {
	if [[ ! -s .config ]]; then
		die "The savedconfig file appears either empty or non-existent (please provide one)"
	fi
	kernel-build_src_configure
}

_apply_patches() {
	local series

	eapply "${WORKDIR}/patch-${PV}"
	_apply_hardened_patch
	series=$(ver_cut 1-2)
	eapply \
		"${FILESDIR}/${series}"/1[5-9]??_*.patch \
		"${FILESDIR}/${series}"/[2-9]???_*.patch
}

_apply_hardened_patch() {
	local upstream_pv

	upstream_pv=${UPSTREAM_PVR%-*}
	_patch_sublevel "${PV}" "${upstream_pv}"
	eapply "${DISTDIR}/linux-hardened-v${UPSTREAM_PVR}.patch"
	_patch_sublevel "${upstream_pv}" "${PV}"
	printf '%s\n' "/^EXTRAVERSION =/s/=.*/= ${KV_LOCALVERSION}/" w q \
	| ed -s Makefile || die "Failed to edit EXTRAVERSION"
}

_patch_sublevel() {
	if [[ "$1" != "$2" ]]; then
		printf '%s\n' "/^SUBLEVEL =/s/=.*/= ${2##*.}/" w q \
		| ed -s Makefile || die "Failed to edit SUBLEVEL"
	fi
}
