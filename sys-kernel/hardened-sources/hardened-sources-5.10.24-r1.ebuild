# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=6
K_SECURITY_UNSUPPORTED=1
K_NOSETEXTRAVERSION=1
ETYPE="sources"

inherit kernel-2
detect_version

DESCRIPTION="Sources that supplement the upstream KSPP changes"
HOMEPAGE="https://github.com/anthraxx/linux-hardened"

MY_PV="${PV}-hardened1"

SRC_URI="
	${KERNEL_URI}
	${HOMEPAGE}/releases/download/${MY_PV}/linux-hardened-${MY_PV}.patch"

KEYWORDS="~alpha amd64 ~arm ~hppa ~ia64 ~mips ~ppc ~ppc64 ~s390 ~sh ~sparc ~x86"
IUSE="+experimental"

src_prepare() {
	local f i extraversion
	declare -a patches
	for f in "${FILESDIR}/${PVR}"/*.patch; do
		i=${f##*/} i=${i%%_*}
		patches[i]=$f
	done
	if ! use experimental; then
		for i in 2904 2911 2912 5013; do
			unset 'patches[i]'
		done
	fi
	eapply "${DISTDIR}/linux-hardened-${MY_PV}.patch"
	eapply "${patches[@]}"
	if [[ $PV != "$PVR" ]]; then
		extraversion="-${PR}"
	fi
	sed -i -e "s/^\(EXTRAVERSION =\).*/\1 -hardened${extraversion}/" Makefile || die
	eapply_user
}
