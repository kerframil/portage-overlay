# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools

DESCRIPTION="Linux Kernel Crypto API User Space Interface Library"
HOMEPAGE="https://www.chronox.de/libkcapi.html"
SRC_URI="https://www.chronox.de/libkcapi/libkcapi-${PV}.tar.xz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="tools"

DEPEND="
	sys-kernel/linux-headers
"
BDEPEND="
	app-text/xmlto
	sys-devel/autoconf
	sys-devel/automake
	sys-devel/m4
	virtual/pkgconfig
"

src_prepare() {
	eapply_user
	eautoreconf
}

src_configure() {
	econf \
		--prefix=/usr \
		--disable-lib-asym \
		--disable-lib-kpp \
		--disable-static \
		$(use_enable tools kcapi-dgstapp) \
		$(use_enable tools kcapi-encapp) \
		$(use_enable tools kcapi-hasher) \
		$(use_enable tools kcapi-rngapp) \
		$(use_enable tools kcapi-speed) \
		$(use_enable tools kcapi-test)
}

src_install() {
	default

	cd -- "${D}" \
	&& find . -name '*.la' -delete \
	|| die

	if ! use tools; then
		rmdir usr/bin
	else
		rm usr/{bin,lib*}/.[!.]* \
		&& rm usr/bin/{fips,md5,sha,sm3}* \
		&& rm -r usr/libexec
	fi || die
}
