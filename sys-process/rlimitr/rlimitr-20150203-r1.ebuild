# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

EGIT_COMMIT="bb781a3a51ca9cec18f889fdd8694dc92884e497"

DESCRIPTION="Get/set resource limits on command line in env(1)-style"
HOMEPAGE="https://github.com/addaleax/rlimitr"
SRC_URI="https://github.com/addaleax/rlimitr/archive/${EGIT_COMMIT}.zip -> ${P}.zip"
RESTRICT="mirror"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~alpha amd64 ~arm ~arm64 ~base ~hppa ~loong ~m68k ~mips ~powerpc ~ppc ~ppc64 ~riscv ~s390 ~sparc ~x86"
IUSE="test"

S="${WORKDIR}/${PN}-${EGIT_COMMIT}"

src_install() {
	dobin rlimitr
	dodoc README.md
}

src_test() {
	./test.sh
}
