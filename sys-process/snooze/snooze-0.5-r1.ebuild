# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="Run a command at a particular time"
HOMEPAGE="https://github.com/leahneukirchen/snooze"
SRC_URI="https://github.com/leahneukirchen/snooze/archive/v${PV}.tar.gz -> ${P}.tar.gz"
RESTRICT="mirror"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="amd64"

src_install() {
	emake PREFIX=/usr DESTDIR="${D}" install
	dodoc NEWS.md README.md
}
