# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

EGIT_REPO_URI="https://src.fedoraproject.org/rpms/systemd.git"
EGIT_COMMIT="a62a7c17ccd3476c7199612f6dcdb0b09a3d4804"
inherit git-r3

DESCRIPTION="Configuration files for systemd-oomd, as provided by Fedora"
HOMEPAGE="https://src.fedoraproject.org/rpms/systemd"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND=">=sys-apps/systemd-247"

src_install() {
	insinto /lib/systemd/oomd.conf.d
	doins 10-oomd-defaults.conf

	insinto /lib/systemd/system/-.slice.d
	doins 10-oomd-root-slice-defaults.conf

	insinto /lib/systemd/system/user@.service.d
	doins 10-oomd-user-service-defaults.conf
}
