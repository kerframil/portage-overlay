# Copyright 1999 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI="7"

DESCRIPTION="Virtual for Java Development Kit (JDK)"
SLOT="1.7"
KEYWORDS="~amd64"
IUSE="headless-awt"

RDEPEND="dev-java/openjdk-bin:7[headless-awt=]"
