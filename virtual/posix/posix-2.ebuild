# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DESCRIPTION="Virtual for providing the POSIX shell and utilities (XCU)"
SLOT="0"
KEYWORDS="amd64 ~x86"

IUSE="c-development cups software-development +user-portability vim xsi"

RDEPEND="
	c-development? (
		sys-devel/bison
		sys-devel/flex
		sys-devel/gcc
	)
	cups? (
		net-print/cups
	)
	software-development? (
		dev-utils/ctags
		sys-devel/make
	)
	user-portability? (
		vim? ( app-editors/vim )
		app-eselect/eselect-vi
		net-misc/inetutils[talkd]
		virtual/cron
	)
	xsi? (
		app-arch/gzip
		app-arch/ncompress
		net-misc/taylor-uucp
		sys-process/psmisc
	)
	app-arch/pax
	app-arch/sharutils
	app-eselect/eselect-sh
	app-shells/dash
	sys-apps/coreutils
	sys-apps/diffutils
	sys-apps/ed
	sys-apps/file
	sys-apps/findutils
	sys-apps/grep
	sys-apps/sed
	sys-apps/util-linux
	sys-devel/bc
	sys-devel/binutils
	sys-devel/m4
	sys-devel/patch
	sys-libs/ncurses
	sys-process/at
	sys-process/procps
	sys-process/time
	virtual/awk
	virtual/libc
	virtual/mailx
	virtual/man"

pkg_postinst() {
	if ! test -e /usr/bin/vi; then
		if use vim; then
			eselect vi set vim
		else
			eselect vi set busybox
		fi
	fi
}
