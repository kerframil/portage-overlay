# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
EAPI=7

DESCRIPTION="Virtual for providing the POSIX shell and utilities (XCU)"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="c-development cups software-development +user-portability xsi"

RDEPEND="
	c-development? (
		app-alternatives/lex
		sys-devel/bison
		sys-devel/gcc
	)
	cups? (
		net-print/cups
	)
	software-development? (
		dev-utils/ctags
		sys-devel/make
	)
	user-portability? (
		|| (
			app-editors/vim
			app-editors/nvim
			app-editors/nvi
			app-editors/elvis
			app-editors/vile
			app-editors/gvim
			app-editors/qvim
			app-editors/xvile
			app-editors/pyvim
			sys-apps/busybox
		)
		app-eselect/eselect-vi
		net-misc/inetutils[talkd]
		virtual/cron
	)
	xsi? (
		app-alternatives/gzip
		app-arch/ncompress
		net-misc/taylor-uucp
		sys-process/psmisc
	)
	app-alternatives/awk[-mawk(-)]
	app-alternatives/bc
	app-alternatives/sh
	app-arch/pax
	app-arch/sharutils
	sys-apps/coreutils
	sys-apps/diffutils
	sys-apps/ed
	sys-apps/file
	sys-apps/findutils
	sys-apps/grep
	sys-apps/sed
	sys-apps/util-linux
	sys-devel/binutils
	sys-devel/m4
	sys-devel/patch
	sys-libs/ncurses
	sys-process/at
	sys-process/procps
	sys-process/time
	virtual/libc
	virtual/mailx
	virtual/man"

pkg_postinst() {
	if use user-portability; then
		eselect vi update --if-unset
	fi
}
