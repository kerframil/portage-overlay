# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit autotools systemd toolchain-funcs

DESCRIPTION="Simple FastCGI wrapper for CGI scripts (CGI support for nginx)"
HOMEPAGE="https://github.com/gnosek/fcgiwrap"
SRC_URI="https://github.com/gnosek/${PN}/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
KEYWORDS="amd64 ~arm ~x86"
SLOT="0"
IUSE="systemd"

RDEPEND="
	dev-libs/fcgi
	systemd? ( sys-apps/systemd )
"
DEPEND="${RDEPEND}"
BDEPEND="
	sys-apps/ed
	virtual/pkgconfig
"

DOCS=( README.rst )

src_prepare() {
	tc-export CC
	eapply "${FILESDIR}"/*.patch
	printf '%s\n' '/man8dir = $(DESTDIR)/s/@prefix@//' w q |
		ed -s Makefile.in || die "ed failed"
	eapply_user
	eautoreconf
}

src_configure() {
	if use systemd; then
		set -- --with-systemd --with-systemdsystemunitdir=
	else
		set -- --without-systemd
	fi
	econf "$@"
}

src_install() {
	default
	systemd_newunit "${FILESDIR}/fcgiwrap-at.service" fcgiwrap@.service
	systemd_newunit "${FILESDIR}/fcgiwrap-at.socket" fcgiwrap@.socket
	newconfd "${FILESDIR}/fcgiwrap.confd" fcgiwrap
}
